// see ISC_license.txt
{$I genLL.inc}
unit GramLexer;

interface

uses
  Sysutils, Runtime;

const
  { token type }
  LEX_APOSTROPHE = 4;
  LEX_ASTERISK = 5;
  LEX_COLON = 6;
  LEX_DIGIT = 7;
  //LEX_ESC = 8;
  LEX_IDENT = 9;
  LEX_LEFT_CURLY_BRACKET = 10;
  LEX_LEFT_PARENTHESIS = 11;
  LEX_LETTER = 12;
  LEX_PLUS_SIGN = 14;
  LEX_QUESTION_MARK = 15;
  LEX_RIGHT_CURLY_BRACKET = 16;
  LEX_RIGHT_PARENTHESIS = 17;
  LEX_STRING_LITERAL = 18;
  LEX_TILDE = 19;
  LEX_VERTICAL_LINE = 20;
  LEX_WS = 21;
  //LEX_XDIGIT = 22;
  LEX_COMMENT = 23;
  LEX_SEMICOLON = 24;
{$IFDEF EDU}
  LEX_EMPTY_LINE = 25;
{$ENDIF}

type
  TGramLexer = class(TLexer)
  protected
    procedure Tokens(); override;
    function GetTokenName(tokenType: integer): string; override;
    procedure mWS();
    procedure mCOMMENT();
    procedure mSTRING_LITERAL();
    procedure mIDENT();
    procedure mCOLON();
    procedure mLEFT_CURLY_BRACKET();
    procedure mRIGHT_CURLY_BRACKET();
    procedure mLEFT_PARENTHESIS();
    procedure mRIGHT_PARENTHESIS();
    procedure mVERTICAL_LINE();
    procedure mQUESTION_MARK();
    procedure mPLUS_SIGN();
    procedure mASTERISK();
    procedure mTILDE();
    procedure mSEMICOLON();
  end;


implementation
uses
  Tokens;

procedure TGramLexer.Tokens;
var
  LA1: integer;
begin
  LA1 := LA(1);
  if LA1 = LEX_EOF then
  begin
    ApproveTokenBuf(LEX_EOF, ceDefault);
    exit;
  end;
  case LA1 of
    9 .. 10, 12 .. 13, ord(' '): mWS();
    ord('/'): if LA(2) = ord('/') then
          mCOMMENT()
      else
          raise ENoViableAltException.Create('');
    ord(''''): mSTRING_LITERAL();
    ord('A') .. ord('Z'), ord('a') .. ord('z'): mIDENT();
    ord(':'): mCOLON();
    ord('|'): mVERTICAL_LINE();
    ord('{'): mLEFT_CURLY_BRACKET();
    ord('}'): mRIGHT_CURLY_BRACKET();
    ord('?'): mQUESTION_MARK();
    ord('+'): mPLUS_SIGN();
    ord('*'): mASTERISK();
    ord('~'): mTILDE();
    ord('('): mLEFT_PARENTHESIS();
    ord(')'): mRIGHT_PARENTHESIS();
    ord(';'): mSEMICOLON();
  else raise ENoViableAltException.Create('');
  end;
end;

procedure TGramLexer.mWS();
var
  LA1: integer;
  cnt: integer;
  cntNewLine: integer;
begin
  cnt := 0;
  cntNewLine := 0;
  repeat
    LA1 := LA(1);
    if (LA1 = 9) or (LA1 = 10) or (LA1 = 12) or (LA1 = 13) or (LA1 = ord(' '))
    then
    begin
      if LA1 = 10 then
          inc(cntNewLine);
      Consume();
    end
    else if cnt >= 1 then
        break
    else ErrorL('');
    inc(cnt);
  until false;
{$IFDEF EDU}
  if cntNewLine >= 2 then
    ApproveTokenBuf(LEX_EMPTY_LINE, ceDefault)
  else
{$endif}
    ApproveTokenBuf(LEX_WS, ceHidden);
end;

procedure TGramLexer.mCOMMENT();
var
  LA1: integer;
begin
  MatchLatin('//');
  repeat
    LA1 := LA(1);
    if (LA1 <> -1) and (LA1 <> 10) and (LA1 <> 13) then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_COMMENT, ceHidden);
end;

procedure TGramLexer.mSTRING_LITERAL();
var
  LA1: integer;
begin
  Consume; // first '
  repeat
    LA1 := LA(1);
    if (LA1 > 0) and (LA1 <> ord('''')) and (LA1 <> 10) and (LA1 <> 13) then
        Consume()
    else if (LA1 = ord('''')) and (LA(2) = ord('''')) then
        Consume(2) //two '
    else
        break;
  until false;
  MatchOne(ord('''')); //last '
  ApproveTokenBuf(LEX_STRING_LITERAL, ceDefault);
end;

procedure TGramLexer.mIDENT();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 >= ord('A')) and (LA1 <= ord('Z')) or (LA1 >= ord('a')) and
      (LA1 <= ord('z'))
      or (LA1 >= ord('0')) and (LA1 <= ord('9')) or (LA1 = ord('_'))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_IDENT, ceDefault);
end;

procedure TGramLexer.mCOLON();
begin
  Consume; //c
  ApproveTokenBuf(LEX_COLON, ceDefault);
end;

procedure TGramLexer.mLEFT_CURLY_BRACKET();
begin
  Consume; // {
  ApproveTokenBuf(LEX_LEFT_CURLY_BRACKET, ceDefault);
end;

procedure TGramLexer.mRIGHT_CURLY_BRACKET();
begin
  Consume; // }
  ApproveTokenBuf(LEX_RIGHT_CURLY_BRACKET, ceDefault);
end;

procedure TGramLexer.mLEFT_PARENTHESIS();
begin
  Consume; // (
  ApproveTokenBuf(LEX_LEFT_PARENTHESIS, ceDefault);
end;

procedure TGramLexer.mRIGHT_PARENTHESIS();
begin
  Consume; // )
  ApproveTokenBuf(LEX_RIGHT_PARENTHESIS, ceDefault);
end;

procedure TGramLexer.mVERTICAL_LINE;
begin
  Consume; // |
  ApproveTokenBuf(LEX_VERTICAL_LINE, ceDefault);
end;

procedure TGramLexer.mQUESTION_MARK;
begin
  Consume; // ?
  ApproveTokenBuf(LEX_QUESTION_MARK, ceDefault);
end;

procedure TGramLexer.mPLUS_SIGN;
begin
  Consume; // +
  ApproveTokenBuf(LEX_PLUS_SIGN, ceDefault);
end;

function TGramLexer.GetTokenName(tokenType: integer): string;
begin
  case tokenType of
    LEX_EOF: result := 'EOF';
    LEX_APOSTROPHE : result := '''';
    LEX_ASTERISK : result := '*';
    LEX_COLON : result := ':';
    LEX_DIGIT : result := '0..9';
    LEX_IDENT : result := 'Ident';
    LEX_LEFT_CURLY_BRACKET : result := '{';
    LEX_LEFT_PARENTHESIS : result := '(';
    LEX_LETTER : result := 'Letter';
    LEX_PLUS_SIGN : result := '+';
    LEX_QUESTION_MARK : result := '?';
    LEX_RIGHT_CURLY_BRACKET : result := '}';
    LEX_RIGHT_PARENTHESIS : result := ')';
    LEX_STRING_LITERAL : result := 'string';
    LEX_TILDE : result := '~';
    LEX_VERTICAL_LINE: result := '|';
    LEX_WS: result := 'White space';
    LEX_COMMENT: result := 'Comment';
    LEX_SEMICOLON: result := ';';
  {$IFDEF EDU}
    LEX_EMPTY_LINE: result := 'Empty line';
  {$ENDIF}
  end;
end;

procedure TGramLexer.mASTERISK();
begin
  Consume; // *
  ApproveTokenBuf(LEX_ASTERISK, ceDefault);
end;

procedure TGramLexer.mTILDE();
begin
  Consume; // ~
  ApproveTokenBuf(LEX_TILDE, ceDefault);
end;

procedure TGramLexer.mSEMICOLON();
begin
  Consume; // ;
  ApproveTokenBuf(LEX_SEMICOLON, ceDefault);
end;

end.
