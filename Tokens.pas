unit Tokens;

interface

uses
  Types;

type
  TChannelEnum = (ceDefault = 1, ceHidden = 2);
  TChannelSet = set of TChannelEnum;

  TTokenBuf = class
  private
    FBuffer: PAnsiChar;
    FOpened: Boolean;
    FPtr: integer;
    FCapacity: integer;
  protected
    function GetValue: UTF8String;
    procedure Grow;
  public
    type_: integer;
    channel: TChannelEnum;
    constructor Create;
    destructor Destroy; override;
    procedure AddChar(ch32: integer);
    procedure Open();
    procedure Close();
    property Opened: Boolean read FOpened;
    property Value: UTF8String read GetValue;
  end;

  IToken = interface ['{8A877724-B126-495A-94DD-907FD4019F7D}']
    function getType: integer;
    function getValue: UTF8String;
    function getChannel: TChannelEnum;
    procedure setCoords(P: TPoint);
    function getCoords: TPoint;
  end;

function MakeIToken(TokenBuf: TTokenBuf): IToken; overload;
function MakeIToken(type_: integer; channel: TChannelEnum): IToken; overload;

implementation
uses
  SysUtils;

const
  INIT_VALUE_SIZE = 1024;

type
  TToken = class(TInterfacedObject, IToken)
  private
    FType: integer;
    FValue: UTF8String;
    FChannel: TChannelEnum;
    FCoords: TPoint;
  public
    constructor Create(TokenBuf: TTokenBuf); overload;
    constructor Create(Atype_: integer; Achannel: TChannelEnum); overload;
    function getType: integer;
    function getValue: UTF8String;
    function getChannel: TChannelEnum;
    procedure setCoords(P: TPoint);
    function getCoords: TPoint;
  end;

function MakeIToken(TokenBuf: TTokenBuf): IToken; overload;
begin
  result := TToken.Create(TokenBuf);
end;

function MakeIToken(type_: integer; channel: TChannelEnum): IToken; overload;
begin
  result := TToken.Create(type_, channel);
end;

{ TTokenBuf }

procedure TTokenBuf.AddChar(ch32: integer);
var
  ch32rec: LongRec absolute ch32;
begin
  if not Opened then
      raise Exception.Create('First open TokenBuf');
  if FPtr + 3 >= FCapacity then
      Grow;
  if ch32 <= $7F then
  begin
    Assert(FPtr < FCapacity);
    FBuffer[FPtr] := AnsiChar(ch32rec.Bytes[0]);
    inc(FPtr);
  end
  else if ch32 <= $7FF then
  begin
    Assert(FPtr + 1 < FCapacity);
    FBuffer[FPtr] := AnsiChar($C0 or (ch32rec.Bytes[1] shl 2) or
      (ch32rec.Bytes[0] shr 6));
    FBuffer[FPtr + 1] := AnsiChar($80 or (ch32rec.Bytes[0] and $3F));
    inc(FPtr, 2);
  end
  else if ch32 <= $FFFF then
  begin
    Assert(FPtr + 2 < FCapacity);
    FBuffer[FPtr] := AnsiChar($E0 or (ch32rec.Bytes[1] shr 4));
    FBuffer[FPtr + 1] := AnsiChar($80 or ((ch32rec.Bytes[1] and $F) shl 2) or
      (ch32rec.Bytes[0] shr 6));
    FBuffer[FPtr + 2] := AnsiChar($80 or (ch32rec.Bytes[0] and $3F));
    inc(FPtr, 3);
  end
  else
  begin
    Assert(FPtr + 3 < FCapacity);
    FBuffer[FPtr] := AnsiChar($F0 or (ch32rec.Bytes[2] shr 2));
    FBuffer[FPtr + 1] := AnsiChar($80 or ((ch32rec.Bytes[2] and 3) shl 4) or
      (ch32rec.Bytes[1] shr 4));
    FBuffer[FPtr + 2] := AnsiChar($80 or ((ch32rec.Bytes[1] and $F) shl 2) or
      (ch32rec.Bytes[0] shr 6));
    FBuffer[FPtr + 3] := AnsiChar($80 or (ch32rec.Bytes[0] and $3F));
    inc(FPtr, 4);
  end;
  Assert(FPtr <= FCapacity);
end;

procedure TTokenBuf.Close;
begin
  FBuffer[FPtr] := #0;
  FOpened := false;
end;

constructor TTokenBuf.Create;
begin
  if INIT_VALUE_SIZE < 4 then
      raise Exception.Create('Minimal TTokenBuf buffer size is 4');
  FCapacity := INIT_VALUE_SIZE;
  GetMem(FBuffer, FCapacity + 1); // +1 to store Zero
end;

destructor TTokenBuf.Destroy;
begin
  FreeMem(FBuffer);
  inherited;
end;

function TTokenBuf.GetValue: UTF8String;
begin
  if Opened then
      raise Exception.Create('First close TokenBuf');
  SetString(result, FBuffer, FPtr);
end;

procedure TTokenBuf.Open;
begin
  if FCapacity > INIT_VALUE_SIZE then
  begin
    FCapacity := INIT_VALUE_SIZE;
    ReallocMem(FBuffer, FCapacity + 1);
  end;
  FPtr := 0;
  FOpened := true;
end;

procedure TTokenBuf.Grow;
begin
  FCapacity := 2 * FCapacity;
  ReallocMem(FBuffer, FCapacity + 1);
end;

{ TToken }

constructor TToken.Create(TokenBuf: TTokenBuf);
begin
  FType := TokenBuf.type_;
  FValue := TokenBuf.Value;
  FChannel := TokenBuf.channel;
end;

constructor TToken.Create(Atype_: integer; Achannel: TChannelEnum);
begin
  FType := Atype_;
  FChannel := Achannel;
end;

function TToken.getChannel: TChannelEnum;
begin
  result := FChannel;
end;

function TToken.getCoords: TPoint;
begin
  result := FCoords;
end;

function TToken.getType: integer;
begin
  result := FType;
end;

function TToken.getValue: UTF8String;
begin
  result := FValue;
end;

procedure TToken.setCoords(P: TPoint);
begin
  FCoords := P;
end;

end.
