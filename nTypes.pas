unit nTypes;

interface
uses
  Types;

type
{$ifdef fpc}
  PBoolean = ^Boolean;
{$endif}
  QuadRec = packed record
    case Byte of
      0: (Quad: int64);
      1: (Lo,Hi: Cardinal);
      2: (DWords: array [0..1] of Cardinal);
      3: (Words: array [0..3] of Word);
      4: (Bytes: array [0..7] of Byte);
  end;
  PQuadRec = ^QuadRec;

  PByteArray = ^TByteArray;
  TByteArray = array[0..MaxInt-1] of Byte;
  DWORDArray = array[0..MaxInt div 4-1] of DWORD;
  PDWORDArray = ^DWORDArray;
  TBoolArray = array[0..MaxInt-1] of boolean;
  PBoolArray = ^TBoolArray;
  TCardinalArray = array [0..MaxInt div 4 - 2] of Cardinal;
  PCardinalArray = ^TCardinalArray;
  TArrayOfCardinal = array of Cardinal;
  TIntegerArray = array [0 .. MaxInt div sizeof(Integer) - 1] of Integer;
  PIntegerArray = ^TIntegerArray;
  {$ifndef UNICODE}UnicodeString = WideString;{$endif}
  {$if not defined(CPUX86) and not defined(CPUX64)}DWORD_PTR = DWORD;{$ifend}

  TPointerList = array [0 .. MaxInt div sizeof(Pointer) - 1] of Pointer;
  PPointerList = ^TPointerList;

implementation

end.
