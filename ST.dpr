// see ISC_license.txt
{$I genLL.inc}
program ST;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  Classes, SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  Trees in 'Trees.pas',
  Tokens in 'Tokens.pas',
  STLexer in 'STLexer.pas',
  STParser in 'STParser.pas';

var
  Stream: TFileStream;
  Lex: TSTLexer;
  Parser: TSTParser;
  Tree: TTree;
begin
  Stream := TFileStream.Create('2.st', fmOpenRead or fmShareDenyNone);
  Lex := TSTLexer.Create(Stream);
  Parser := TSTParser.Create(Lex);
  Tree:=Parser.syn_program();
  Tree.print(0);
  Tree.Free;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
