// see ISC_license.txt
{$I genLL.inc}
unit SimpleLexer;

interface

uses
  Sysutils, Runtime;

const
  LEX_ID = 1;
  LEX_INT = 2;
  LEX_WS = 3;
  LEX_LCURLY = 4;
  LEX_RCURLY = 5;
  LEX_COMMENT = 6;
  LEX_JAVADOC = 7;
  LEX_LPAREN = 8;
  LEX_RPAREN = 9;
  LEX_SEMI = 10;
  LEX_ASSIGN = 11;
  LEX_KEY_INT = 12;
  LEX_KEY_METHOD = 13;
  LEX_KEY_RETURN = 14;

type
  TSimpleLexer = class(TLexer)
  protected
    procedure Tokens(); override;
    procedure KeyWords();
    function GetTokenName(tokenType: integer): string; override;
    function fENDL(): boolean;
    function fLETTER(): boolean;
    function fDIGIT(): boolean;
    procedure mWS();
    procedure mCOMMENT();
    procedure mJAVADOC();
    procedure mID();
    procedure mINT();
    procedure mKEY_INT();
    procedure mKEY_METHOD();
    procedure mKEY_RETURN();
  end;

implementation
uses
  Tokens;

procedure TSimpleLexer.Tokens;
var
  LA1: integer;
  lextype: integer;
begin
  LA1 := LA(1);
  if LA1 = LEX_EOF then
  begin
    ApproveTokenBuf(LEX_EOF, ceDefault);
    exit;
  end;
  case LA1 of
    9 .. 10, 13, ord(' '): mWS();
    ord('A') .. ord('Z'), ord('a') .. ord('z'): KeyWords();
    ord('0') .. ord('9'): mINT();
    ord('/'): if LA(2) = ord('*') then
      begin
        if LA(3) = ord('*') then
            mJAVADOC()
        else
            mCOMMENT()
      end
      else
          raise ENoViableAltException.Create('');
    ord('{'), ord('}'), ord('('), ord(')'), ord(';'), ord('='):
      begin
        case LA1 of
          ord('{'): lextype := LEX_LCURLY;
          ord('}'): lextype := LEX_RCURLY;
          ord('('): lextype := LEX_LPAREN;
          ord(')'): lextype := LEX_RPAREN;
          ord(';'): lextype := LEX_SEMI;
          ord('='): lextype := LEX_ASSIGN;
        end;
        Consume;
        ApproveTokenBuf(lextype, ceDefault);
      end;
  end;
end;

function TSimpleLexer.GetTokenName(tokenType: integer): string;
begin
  case tokenType of
    LEX_ID: result := 'ID';
    LEX_INT: result := 'int';
    LEX_WS: result := 'whitespace';
    LEX_LCURLY: result := '{';
    LEX_RCURLY: result := '}';
    LEX_COMMENT: result := 'comment';
    LEX_JAVADOC: result := 'javadoc';
    LEX_LPAREN: result := '(';
    LEX_RPAREN: result := ')';
    LEX_SEMI: result := ';';
    LEX_ASSIGN: result := '=';
    LEX_KEY_INT: result := 'key int';
    LEX_KEY_METHOD: result := 'key method';
    LEX_KEY_RETURN: result := 'key return';
  end;
end;

procedure TSimpleLexer.KeyWords;
begin
  case LA(1) of
    ord('i'):
      begin
        if CompareLatin('int') then
            mKEY_INT()
        else
            mID();
      end;
    ord('m'):
      begin
        if CompareLatin('method') then
            mKEY_METHOD()
        else
            mID();
      end;
    ord('r'):
      begin
        if CompareLatin('return') then
            mKEY_RETURN()
        else
            mID();
      end;
  else
    mID();
  end;
end;

function TSimpleLexer.fENDL(): boolean;
begin
  result := (LA(1) = 13) and (LA(2) = 10) or (LA(1) = 10);
end;

function TSimpleLexer.fLETTER(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('A')) and (ch <= ord('Z'))
    or (ch >= ord('a')) and (ch <= ord('z'))
end;

function TSimpleLexer.fDIGIT(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('0')) and (ch <= ord('9'));
end;

procedure TSimpleLexer.mWS();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 = 9) or fENDL() or (LA1 = ord(' '))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_WS, ceHidden);
end;

procedure TSimpleLexer.mCOMMENT;
var
  LA1: integer;
begin
  MatchLatin('/*');
  repeat
    LA1 := LA(1);
    if LA1 = -1 then
        raise ETokenEOFException.Create('');
    if LA1 <> ord('*') then
        Consume()
    else
    begin
      if LA(2) = ord('/') then
          break
      else
          Consume(2);
    end;
  until false;
  ApproveTokenBuf(LEX_COMMENT, ceHidden);
end;

procedure TSimpleLexer.mJAVADOC;
var
  LA1: integer;
begin
  MatchLatin('/**');
  repeat
    LA1 := LA(1);
    if (LA1 <> -1) and (LA1 <> ord('*')) then
        Consume()
    else
    begin
      Consume();
      if LA(1) = ord('/') then
      begin
        Consume();
        break
      end;
    end;
  until false;
  ApproveTokenBuf(LEX_COMMENT, ceHidden);
end;

procedure TSimpleLexer.mID();
var
  LA1: integer;
begin
  if fLETTER() or (LA(1) = ord('_')) then
      Consume;
  repeat
    LA1 := LA(1);
    if fLETTER() or (LA(1) = ord('_')) or fDIGIT() then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_ID, ceDefault);
end;

procedure TSimpleLexer.mINT();
var
  LA1: integer;
begin
  if LA(1) = ord('0') then
      Consume()
  else
  begin
    Consume(); // 1..9
    while fDIGIT() do
        Consume();
  end;
  ApproveTokenBuf(LEX_INT, ceDefault);
end;

procedure TSimpleLexer.mKEY_INT;
begin
  MatchLatin('int');
  ApproveTokenBuf(LEX_KEY_INT, ceDefault);
end;

procedure TSimpleLexer.mKEY_METHOD;
begin
  MatchLatin('method');
  ApproveTokenBuf(LEX_KEY_METHOD, ceDefault);
end;

procedure TSimpleLexer.mKEY_RETURN;
begin
  MatchLatin('return');
  ApproveTokenBuf(LEX_KEY_RETURN, ceDefault);
end;

end.
