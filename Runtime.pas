// see ISC_license.txt
{$I genLL.inc}
unit Runtime;

interface

uses
  Types, Classes, SysUtils, nQueues, Tokens;

const
  CHAR_EOF = -1;
  LEX_EOF = -1;
  LEXNONE = 0;

type
  ERecognitionException = class(Exception);
  ENoViableAltException = class(ERecognitionException); // need to remove
  EMismatchedCharException = class(ERecognitionException); // need to insert
  ETokenEOFException = class(ERecognitionException);

  TLexer = class
  private
    FCharQueue: TCharQueue;
  protected
    FTokenBuf: TTokenBuf;
    procedure Tokens(); virtual; abstract;
    procedure OnCreate(); virtual;
    function LA(k: integer): integer; // -1 = End of file
    procedure Consume(n: integer = 1);
    procedure Reject();
    procedure MatchOne(ch: Cardinal);
    procedure MatchRange(chFrom,chTo: Cardinal);
    procedure MatchLatin(const str: AnsiString);
    function CompareLatin(const str: AnsiString): Boolean;
    function CompareConsumeLatin(const str: AnsiString): Boolean;
    procedure ApproveTokenBuf(AType: integer; AChannel: TChannelEnum);
    procedure ErrorL(err: string);
  public
    LastCoords: TPoint;
    constructor Create(AInput: TStream); overload;
    constructor Create(AString: string); overload;
    destructor Destroy; override;
    function NextToken(ChannelFilter: TChannelSet): IToken;
    function GetTokenName(tokenType: integer): string; virtual; abstract;
  end;

//Queue with priority
  TTokenQueue = class
  private
    FNormalTokens: TInterfaceQueue;
    FRecoveryTokens: TIntQueue;
    function GetCount(): integer; {$IFDEF USEINLINE}inline; {$ENDIF}
  public
    constructor Create;
    destructor Destroy; override;
    procedure Push(AItem: IToken); {$IFDEF USEINLINE}inline; {$ENDIF}
    procedure PushRecovery(const type_: integer); {$IFDEF USEINLINE}inline; {$ENDIF}
    function Pop(PCanPop: PBoolean=nil): IToken;
    procedure PopAndFree(PCanPop: PBoolean=nil);
    function IsEmpty: Boolean; {$IFDEF USEINLINE}inline; {$ENDIF}
    function Head(fromHead: Cardinal = 0): IToken;
    property Count: integer read GetCount;
  end;

  TParser = class
  private
    FTokenQueue: TTokenQueue;
  protected
    FLexer: TLexer;
    function Eq(lookahead, type_: integer): Boolean; virtual;
    function LT(k: integer): IToken;
    function LA(k: integer): integer; {$IFDEF USEINLINE}inline; {$ENDIF}
    function Consume(): IToken;
    procedure ConsumeToType(const type_: integer);
    function Match(const type_: integer): IToken;
    procedure RecoveryInsert(const type_: integer); {$IFDEF USEINLINE}inline; {$ENDIF}
    procedure ErrorP(err: string);
  public
    constructor Create(ALexer: TLexer);
    destructor Destroy; override;
  end;

implementation
uses
  Math;

{ TLexer }
const
  MaxBufSize = 256 * 1024;

procedure TLexer.ApproveTokenBuf(AType: integer; AChannel: TChannelEnum);
begin
  FTokenBuf.type_ := AType;
  FTokenBuf.channel := AChannel;
end;

procedure TLexer.Consume(n: integer = 1);
var
  d32: Cardinal;
  i: integer;
begin
  for i:=0 to n-1 do
  begin
    d32 := FCharQueue.Pop();
    FTokenBuf.AddChar(d32);
  end;
end;

//need to error recovery
procedure TLexer.Reject();
begin
  FCharQueue.Pop();
end;

procedure TLexer.MatchOne(ch: Cardinal);
var
  d32: Cardinal;
  CanPop: Boolean;
begin
  d32 := FCharQueue.Pop(@CanPop);
  if (not CanPop) or (ch <> d32) then
  begin
    ErrorL(Format('Expected %s is %s',[string(WideChar(ch)),string(WideChar(d32))]));
  end else
      FTokenBuf.AddChar(d32);
end;

procedure TLexer.MatchRange(chFrom,chTo: Cardinal);
var
  d32: Cardinal;
  CanPop: Boolean;
begin
  d32 := FCharQueue.Pop(@CanPop);
  if (not CanPop) or (d32<chFrom) or (d32>chTo) then
  begin
    ErrorL(Format('Expected range [''%s''-''%s''] is ''%s''',
      [string(WideChar(chFrom)),string(WideChar(chTo)),string(WideChar(d32))]));
  end else
      FTokenBuf.AddChar(d32);
end;

procedure TLexer.MatchLatin(const str: AnsiString);
var
  ch, d32: Cardinal;
  CanPop: Boolean;
  i: integer;
begin
  for i:=1 to Length(str) do
  begin
    ch := Cardinal(str[i]);
    d32 := FCharQueue.Pop(@CanPop);
    if (not CanPop) or (ch <> d32) then
    begin
      raise EMismatchedCharException.CreateFmt('lexer expected %s is %s',
        [string(WideChar(ch)),string(WideChar(d32))]);
    end else
        FTokenBuf.AddChar(d32);
  end;
end;

function TLexer.CompareLatin(const str: AnsiString): Boolean;
var
  i: integer;
begin
  result := true;
  for i:=1 to Length(str) do
  begin
    if Cardinal(str[i])<>FCharQueue[i-1] then
    begin
      result := false;
      exit;
    end;
  end;
end;

function TLexer.CompareConsumeLatin(const str: AnsiString): Boolean;
var
  i: integer;
begin
  result := true;
  for i:=1 to Length(str) do
  begin
    if Cardinal(str[i])<>FCharQueue[i-1] then
    begin
      result := false;
      exit;
    end;
  end;
  Consume(Length(str));
end;

constructor TLexer.Create(AInput: TStream);
var
  remaining: int64;
begin
  remaining:=AInput.Size-AInput.Position;
  FCharQueue := TCharQueue.Create(AInput, min(remaining,MaxBufSize));
  FTokenBuf := TTokenBuf.Create;
  OnCreate();
end;

constructor TLexer.Create(AString: string);
begin
  FCharQueue := TCharQueue.Create(AString);
  FTokenBuf := TTokenBuf.Create;
  OnCreate();
end;

destructor TLexer.Destroy;
begin
  FTokenBuf.Free;
  FCharQueue.Free;
  //don't  free FStream
  inherited;
end;

procedure TLexer.ErrorL(err: string);
begin
  writeln(Format('L: Error [%d,%d] %s',[LastCoords.Y,LastCoords.X,err]));
end;

function TLexer.LA(k: integer): integer;
begin
  result := FCharQueue[k - 1];
  LastCoords := FCharQueue.PeekCoords.P;
end;

function TLexer.NextToken(ChannelFilter: TChannelSet): IToken;
var
  Coords: TPoint;
begin
  repeat
    FTokenBuf.Open;
    try
      LA(1); //enforce getting first character of token for Coords
    except
      on E: EndOfStreamException do
      begin
          result := MakeIToken(LEX_EOF, ceDefault);
          exit;
      end;
    end;
    Coords := FCharQueue.PeekCoords.P;
    try
        Tokens();
        FTokenBuf.Close;
    except
      on E: ENoViableAltException do
      begin
        ErrorL('error No Viable Alt');
        Reject(); //first thing to do: do not close FTokenBuf
      end;
      on E: EndOfStreamException do
      begin
          result := MakeIToken(LEX_EOF, ceDefault);
          FTokenBuf.Close;
          result.setCoords(Coords);
          exit;
      end;
    end;
  until (FTokenBuf.channel in ChannelFilter) and (not FTokenBuf.Opened);
  result := MakeIToken(FTokenBuf);
  result.setCoords(Coords);
  //writeln(result.getValue);
end;

procedure TLexer.OnCreate;
begin
end;

{ TParser }

constructor TParser.Create(ALexer: TLexer);
begin
  FLexer := ALexer;
  FTokenQueue := TTokenQueue.Create;
end;

destructor TParser.Destroy;
begin
  FTokenQueue.Free;
  inherited;
end;

//will subclassed with Schrodinger tokens
function TParser.Eq(lookahead, type_: integer): Boolean;
begin
  result := lookahead = type_;
end;

procedure TParser.ErrorP(err: string);
var
  token: IToken;
  Coords: TPoint;
begin
  token:=LT(1);
  if token.getType = LEX_EOF then
    raise ETokenEOFException.Create('');
  Coords := token.getCoords;
  writeln(Format('P: Error [%d,%d] %s',[Coords.Y, Coords.X, err]));
  Consume();
end;

function TParser.LA(k: integer): integer;
begin
  result := LT(k).GetType;
end;

function TParser.LT(k: integer): IToken;
var
  Index: integer;
begin
  index := k - 1;
  if index < FTokenQueue.Count then
  begin
    result := FTokenQueue.Head(index);
    exit;
  end
  else
    if index > FTokenQueue.Count then
      raise Exception.Create('LAtoken can''t jump over token');

  Assert(index = FTokenQueue.Count);
  result := FLexer.NextToken([ceDefault]);
  FTokenQueue.Push(result);
end;

function TParser.Consume(): IToken;
var
  token: IToken;
begin
  if FTokenQueue.IsEmpty then
  begin
    result := FLexer.NextToken([ceDefault]);
  end else
      result := FTokenQueue.Pop();
end;

//up to type or EOF for recovery
procedure TParser.ConsumeToType(const type_: integer);
var
  token: IToken;
begin
  repeat
     if FTokenQueue.IsEmpty then
     begin
       token := FLexer.NextToken([ceDefault]);
       if Eq(token.getType, type_) or (token.getType = LEX_EOF) then
       begin
         FTokenQueue.Push(token);
         break;
       end;
     end else
     begin
       token := FTokenQueue.Head(0);
       if Eq(token.getType, type_) or (token.getType = LEX_EOF) then
         break
       else FTokenQueue.PopAndFree();
     end;
  until false;
end;

function TParser.Match(const type_: integer): IToken;
begin
  result := LT(1);
  if Eq(result.getType, type_) then
  begin
    FTokenQueue.PopAndFree();
  end else
  begin
    ErrorP(Format('expected %s is %s',[
    FLexer.GetTokenName(type_), FLexer.GetTokenName(LA(1))
    ]));
  end;
end;

procedure TParser.RecoveryInsert(const type_: integer);
begin
   FTokenQueue.PushRecovery(type_);
end;

{ FTokenQueue }

constructor TTokenQueue.Create;
begin
  FNormalTokens := TInterfaceQueue.Create();
  FRecoveryTokens := TIntQueue.Create(false);
end;

destructor TTokenQueue.Destroy;
begin
  FNormalTokens.Free;
  FRecoveryTokens.Free;
  inherited;
end;

function TTokenQueue.GetCount: integer;
begin
  result := FRecoveryTokens.Count + FNormalTokens.Count;
end;

function TTokenQueue.Head(fromHead: Cardinal): IToken;
var
  rc: integer;
begin
  rc := FRecoveryTokens.Count;
  if fromHead<rc then
    result := MakeIToken(FRecoveryTokens.Head(fromHead), ceDefault)
  else
    result := FNormalTokens.Head(fromHead-rc) as IToken;
end;

function TTokenQueue.IsEmpty: Boolean;
begin
  result := FNormalTokens.IsEmpty and FRecoveryTokens.IsEmpty;
end;

function TTokenQueue.Pop(PCanPop: PBoolean): IToken;
begin
  if FRecoveryTokens.IsEmpty then
    result := FNormalTokens.Pop(PCanPop) as IToken
  else
    result := MakeIToken(FRecoveryTokens.Pop(PCanPop), ceDefault);
end;

procedure TTokenQueue.PopAndFree(PCanPop: PBoolean);
begin
  if FRecoveryTokens.IsEmpty then
    FNormalTokens.PopAndFree(PCanPop)
  else
    FRecoveryTokens.Pop(PCanPop);
end;

procedure TTokenQueue.Push(AItem: IToken);
begin
  FNormalTokens.Push(AItem);
end;

procedure TTokenQueue.PushRecovery(const type_: integer);
begin
  FRecoveryTokens.Push(type_);
end;

end.
