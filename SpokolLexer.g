lexer grammar SpokolLexer;

WS  :  (' '|'\r'|'\t'|'\n') {channel=HIDDEN;}
    ;

COMMENT : '{' (~'}')* '}' {channel=HIDDEN;}
        ;

STRING_LITERAL
    :  '\'' ( EscapeSequence | ~('\\'|'\'') )* '\''
    ;

EscapeSequence
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    ;

COMMA: ',' ;
PARENTHESIS_OPEN: '(' ;
PARENTHESIS_CLOSE: ')' ;
SEMICOLON: ';' ;
PLUS: '+' ;
MINUS: '-' ;
EQUAL: '=' ;
MULTIPLY: '*' ;
DIVIDE: '/' ;
NOT_EQUAL: '!=' ;
LESS_EQUAL: '<=' ;
LESS: '<' ;
GREATER_EQUAL: '>=' ;
GREATER: '>' ;
ASSIGN: ':=' ;
COLON: ':';
CONST_INT: ('0'..'9')+ ;
FLOATING_POINT_LITERAL:
        ('0'..'9')+ '.' ('0'..'9')+ ('e'|'E') ('+'|'-')? ('0'..'9')+
    |   ('0'..'9')+ ('e'|'E') ('+'|'-')? ('0'..'9')+
    |   ('0'..'9')+ '.' ('0'..'9')+
    ;
AND: 'AND' ;
OR: 'OR' ;
NOT: 'not' ;
STRING: 'string' ;
INT: 'int' ;
BOOL: 'bool' ;
REAL: 'real' ;
PROGRAM: 'program' ;
PROCEDURE: 'procedure' ;
BEGIN: 'begin' ;
END: 'end' ;
DO: 'do' ;
ENDWHILE: 'endwhile' ;
BREAK: 'break' ;
IF: 'if' ;
ELSE: 'else' ;
ENDIF: 'endif' ;
PRINT: 'print' ;
READ: 'read' ;
THEN: 'then' ;
VAR: 'var' ;
WHILE: 'while' ;
IDENT: ('A'..'Z'|'a'..'z'|'_')('A'..'Z'|'a'..'z'|'_'|'0'..'9')* ;