unit TestNoNewlineTemplates;

interface

implementation
uses
  TestFramework;

type
  TTestNoNewlineTemplates = class(TTestcase)
  published
    procedure testNoNewlineTemplate; virtual;
  end;

procedure TTestNoNewlineTemplates.testNoNewlineTemplate;
var
  template,expected: string;
begin
  template :=
      't(x) ::= <%'#10 +
			'[  <if(!x)>' +
			'<else>' +
			'<x>'#10 +
			'<endif>' +
			#10 +
			#10 +
			']'#10 +
			#10 +
			'%>'#10;
  {
  STGroup g = new STGroupString(template);
	ST st = g.getInstanceOf("t");
	st.add("x", 99);
	expected := "[  99]";
	String result = st.render();
	check(expected=result,'error');
  check(false, 'Unit1 error');}
end;


initialization
  // One possible way to register a test
  TestFramework.registerTest('', TTestNoNewlineTemplates.Suite);
end.
