// see ISC_license.txt
{$I genLL.inc}
unit SchrodingerParser;

interface

uses
  Runtime, SchrodingerLexer;

type
  TSchrodingerParser = class(TParser)
  protected
    function Eq(lookahead, type_: integer): Boolean; override;
  public
    procedure pars_program;
    procedure pars_stmt;
    procedure pars_assignmentStmt;
    procedure pars_ifStmt;
    procedure pars_expression;
    procedure pars_term;
  end;

implementation

procedure TSchrodingerParser.pars_program;
begin
 try
   while LA(1) <> LEX_EOF do
      pars_stmt();
 except
   on E: ETokenEOFException do writeln('Unexpected EOF');
 end;
end;

procedure TSchrodingerParser.pars_stmt;
var
  LA1: integer;
begin
  LA1 := LA(1);
  if LA1=LEX_IF then
    pars_ifStmt
  else if Eq(LA1, LEX_ID) then
    pars_assignmentStmt
  else
    ErrorP('');
end;

procedure TSchrodingerParser.pars_term;
var
  LA1: integer;
begin
  LA1 := LA(1);
  if Eq(LA1, LEX_ID)or (LA1 = LEX_INT) then
      Consume()
  else
    ErrorP('');
end;

procedure TSchrodingerParser.pars_expression;
var
  LA1: integer;
begin
  pars_term();
  LA1:= LA(1);
  if (LA1 = LEX_EQUALS) or (LA1 = LEX_NOT_EQUALS) then
      Consume()
  else
    ErrorP('');
  pars_term();
end;

procedure TSchrodingerParser.pars_ifStmt;
begin
  Match(LEX_IF);
  pars_expression();
  Match(LEX_THEN);
  pars_stmt();
end;

function TSchrodingerParser.Eq(lookahead, type_: integer): Boolean;
begin
  if type_ = MaskHi then
    result := (lookahead <> LEX_EOF)and(lookahead and MaskHi<>0)
  else
    result := lookahead = type_;
end;

procedure TSchrodingerParser.pars_assignmentStmt;
begin
  Match(LEX_ID);
  Match(LEX_ASSIGNMENT);
  pars_term;
  Match(LEX_SEMI);
end;

end.
