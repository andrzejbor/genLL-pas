// see ISC_license.txt
{$I genLL.inc}
unit GramParser;

interface

uses
  Runtime, GramLexer;

type
  TGramParser = class(TParser)
  public
    procedure predicate();
    procedure token();
    procedure repetition();
    procedure rule();
    procedure nontermlhs();
    procedure rules();
    procedure ntermrules();
{$IFDEF EDU} procedure grammar(); {$ENDIF}
    procedure document();
  end;

implementation
uses
  SysUtils;

{ TGramParser }
procedure TGramParser.nontermlhs;
begin
  Match(LEX_IDENT);
end;

procedure TGramParser.ntermrules();
begin
  nontermlhs();
  Match(LEX_COLON);
  rules();
  Match(LEX_SEMICOLON);
end;

procedure TGramParser.predicate;
begin
  Match(LEX_LEFT_CURLY_BRACKET);
  Match(LEX_IDENT);
  Match(LEX_RIGHT_CURLY_BRACKET);
  Match(LEX_QUESTION_MARK);
end;

procedure TGramParser.repetition;
var
  LA1: integer;
begin
  LA1 := LA(1);
  if (LA1 = LEX_ASTERISK) or (LA1 = LEX_PLUS_SIGN) or (LA1 = LEX_QUESTION_MARK) then
    Consume() // to zostawic bo na pewno
  else ErrorP('MismatchedSet: * or + or ?');
end;

// rule: predicate? (token repetition? )+;
procedure TGramParser.rule;
var
  LA1: integer;
  cnt: integer;
begin
  if LA(1) = LEX_LEFT_CURLY_BRACKET then
    predicate();
  cnt := 0;
  repeat
    LA1 := LA(1);
    if (LA1 = LEX_IDENT) or (LA1 = LEX_LEFT_PARENTHESIS) or (LA1 = LEX_TILDE) or
      (LA1 = LEX_STRING_LITERAL) then
    begin
      token();
      LA1 := LA(1);
      if (LA1 = LEX_ASTERISK) or (LA1 = LEX_PLUS_SIGN) or (LA1 = LEX_QUESTION_MARK) then
        repetition();
    end
  else if cnt >= 1 then
    break
  else
    ErrorP('Syntax error');
  inc(cnt);
  until false;
end;

procedure TGramParser.rules;
begin
  rule();
  repeat
    if LA(1) = LEX_VERTICAL_LINE then
    begin
      Consume();
      rule();
    end else break;
  until false;
end;

procedure TGramParser.token;
var
  LA1: integer;
begin
  LA1 := LA(1);
  case LA1 of
    LEX_IDENT:
      Match(LEX_IDENT);
    LEX_TILDE, LEX_STRING_LITERAL:
      begin
        if LA1 = LEX_TILDE then
          Match(LEX_TILDE);
        Match(LEX_STRING_LITERAL);
      end;
    LEX_LEFT_PARENTHESIS:
      begin
        Match(LEX_LEFT_PARENTHESIS);
        rules();
        Match(LEX_RIGHT_PARENTHESIS);
      end;
  else
    ErrorP('MismatchedSet');
  end;
end;

{$IFDEF EDU} procedure TGramParser.grammar();
var
  LA1: integer;
  cnt: integer;
begin
  cnt := 0;
  repeat
    LA1 := LA(1);
    if (LA1 = LEX_EOF)or(LA1 = LEX_EMPTY_LINE) then
        break;
    if LA1 <> LEX_IDENT then
    begin
      ErrorP(Format('expected IDENT is %s',[
            FLexer.GetTokenName(LA1) ]));
      ConsumeToType(LEX_IDENT);
    end;
    ntermrules();
    inc(cnt);
  until false;
end;
{$ENDIF}

procedure TGramParser.document;
var
  LA1: integer;
  cnt: integer;
begin
  try
    cnt := 0;
    repeat
      LA1 := LA(1);
      if LA1 = LEX_EOF then
          break;
      if LA1 <> LEX_IDENT then
      begin
        ErrorP('error in document');
        if LA(1) = LEX_COLON then
            RecoveryInsert(LEX_IDENT)
        else
            ConsumeToType(LEX_IDENT);
      end;
{$IFDEF EDU}
      grammar();
      if LA(1) <> LEX_EOF then
          Match(LEX_EMPTY_LINE);
{$ELSE}
      ntermrules();
{$ENDIF}
      inc(cnt);
    until false;
  except
    on E: ETokenEOFException do
      writeln('Unexpected EOF');
  end;
end;

end.


   while LA(1) <> LEX_EOF do
      pars_stmt();
