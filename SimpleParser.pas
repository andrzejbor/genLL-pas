// see ISC_license.txt
{$I genLL.inc}
unit SimpleParser;

interface

uses
  Runtime, SimpleLexer;

type
  TSimpleParser = class(TParser)
  public
    procedure pars_program;
    procedure pars_variable;
    procedure pars_method;
    procedure pars_block;
    procedure pars_expr;
    procedure pars_statement;
  end;

implementation

procedure TSimpleParser.pars_program;
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if LA1 = LEX_KEY_INT then
        pars_variable()
    else if LA1 = LEX_KEY_METHOD then
        break
    else
    begin // also EOF is error
      ErrorP('error in program');
      ConsumeToType(LEX_SEMI);
      Consume(); // SEMI or EOF
    end;
  until false;
  repeat
      pars_method();
  until LA(1) = LEX_EOF;
end;

procedure TSimpleParser.pars_variable();
var
  LA1: integer;
begin
  Match(LEX_KEY_INT);
  Match(LEX_ID);
  LA1 := LA(1);
  if LA1 = LEX_ASSIGN then
  begin
    Match(LEX_ASSIGN);
    pars_expr;
  end;
  Match(LEX_SEMI);
end;

procedure TSimpleParser.pars_method;
begin
  Match(LEX_KEY_METHOD);
  Match(LEX_ID);
  Match(LEX_LPAREN);
  Match(LEX_RPAREN);
  pars_block();
end;

procedure TSimpleParser.pars_block;
begin
  Match(LEX_LCURLY);
  while LA(1) = LEX_KEY_INT do
      pars_variable();
  repeat
      pars_statement();
  until LA(1) = LEX_RCURLY;
  Match(LEX_RCURLY);
end;

procedure TSimpleParser.pars_statement;
var
  LA1: integer;
begin
  LA1 := LA(1);
  case LA1 of
    LEX_ID:
      begin
        Match(LEX_ID);
        Match(LEX_ASSIGN);
        pars_expr;
        Match(LEX_SEMI);
      end;
    LEX_KEY_RETURN:
      begin
        Match(LEX_KEY_RETURN);
        pars_expr;
        Match(LEX_SEMI);
      end;
    LEX_LCURLY: pars_block();
  else ErrorP('');
  end;
end;

procedure TSimpleParser.pars_expr;
var
  LA1: integer;
begin
  LA1 := LA(1);
  case LA1 of
    LEX_ID: Match(LEX_ID);
    LEX_INT: Match(LEX_INT);
  else ErrorP('');
  end;
end;

end.
