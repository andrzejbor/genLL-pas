unit nQueues;

interface
uses
  Windows, Classes, SysUtils, nTypes;

type
  EndOfStreamException = class(Exception);
  TIntStack = class
  private
    FList: PIntegerArray;
    FCapacity: Cardinal;
    FShrinkTrigger: Cardinal;
    FTop: Cardinal;
  protected
    function GetCount(): Integer; {$IFDEF USEINLINE}inline; {$ENDIF}
    procedure Grow;
    procedure Shrink();
  public
    destructor Destroy; override;
    procedure Assign(SrcList: TIntStack);
    function AtLeast(ACount: Integer): Boolean; {$IFDEF USEINLINE}inline;{$ENDIF}
    procedure Push(AItem: integer);
    function Pop(PCanPop: PBoolean=nil): integer;
    function IsEmpty: Boolean; {$IFDEF USEINLINE}inline; {$ENDIF}
    function Top(fromTop: Cardinal = 0): integer;
    function Bottom(fromBottom: Cardinal = 0): integer;
    procedure Clear;
    property Count: Integer read GetCount;
  end;

  // Queue: items are added to tail and are removed from the Head.
  // if FHead=FTail -> Queue empty
  // if FHead<FTail -> normal situation, Count=FTail-FHead, data: FHead..FTail-1
  // if FHead>FTail -> wrapping on cyclic buffer, Count=FCapacity-(FHead-FTail), data: FHead..Capacity-1 + 0..FTail-1
  // if we Push and will FHead=FTail, it must be Grow
  // if FHead<=FTail we Grow and Shrink normally
  // if FHead>FTail and Grow - copy first buffer position to end, dec FTail or wrap it to end
  // if FHead>FTail and Shrink - dec Head, move itema after head, decreease gap
  TIntQueue = class
  private
    FList: PIntegerArray;
    FCapacity: Cardinal;
    FShrinkTrigger: Cardinal;
    FHead: Cardinal;
    FTail: Cardinal;
    FFreeObjects: Boolean;
  protected
    function GetCount(): integer; {$IFDEF USEINLINE}inline; {$ENDIF}
    procedure Grow;
    procedure Shrink();
    function HeadRef(fromHead: Cardinal = 0): PInteger;
    function TailRef(fromTail: Cardinal): PInteger;
  public
    constructor Create(bFreeObjects: Boolean);
    destructor Destroy; override;
    procedure Assign(SrcList: TIntQueue);
    function AtLeast(ACount: integer): Boolean; {$IFDEF USEINLINE}inline;{$ENDIF}
    function Push(AItem: integer): PInteger;
    function Pop(PCanPop: PBoolean=nil): integer;
    procedure PopAndFree(PCanPop: PBoolean=nil);
    function IsEmpty: Boolean; {$IFDEF USEINLINE}inline; {$ENDIF}
    function Head(fromHead: Cardinal = 0): integer; {$IFDEF USEINLINE}inline;{$ENDIF}
    function Tail(fromTail: Cardinal = 0): integer; {$IFDEF USEINLINE}inline;{$ENDIF}
    procedure Clear;
    property Count: integer read GetCount;
  end;

  TThreadQueue = class
  private
    FLock: TRTLCriticalSection;
    FQueue: TIntQueue;
  protected
    function GetCount(): integer; {$IFDEF USEINLINE}inline; {$ENDIF}
  public
    constructor Create(bFreeObjects: Boolean);
    destructor Destroy; override;
    procedure Assign(SrcList: TThreadQueue);
    function AtLeast(ACount: integer): Boolean; {$IFDEF USEINLINE}inline;{$ENDIF}
    procedure Push(AItem: integer);
    function Pop(PCanPop: PBoolean=nil): integer;
    procedure PopAndFree(PCanPop: PBoolean=nil);
    //isEmpty is dangerous because betwen isEmpty and Pop can other thread Pop
    function Head(fromHead: Cardinal = 0): integer;
    function Tail(fromTail: Cardinal = 0): integer;
    procedure Clear;
    property Count: integer read GetCount;
  end;

  TInterfaceQueue = class
  private
    FQueue: TIntQueue;
  protected
    function GetCount(): integer; {$IFDEF USEINLINE}inline; {$ENDIF}
    procedure ClearIntf;
  public
    constructor Create();
    destructor Destroy; override;
    function AtLeast(ACount: integer): Boolean; {$IFDEF USEINLINE}inline;{$ENDIF}
    procedure Push(AItem: IInterface);
    function Pop(PCanPop: PBoolean=nil): IInterface;
    procedure PopAndFree(PCanPop: PBoolean=nil);
    function IsEmpty: Boolean; {$IFDEF USEINLINE}inline; {$ENDIF}
    function Head(fromHead: Cardinal = 0): IInterface;
    function Tail(fromTail: Cardinal = 0): IInterface;
    procedure Clear;
    property Count: integer read GetCount;
  end;

  TEncodingEnum = (eeUTF8, eeUTF16LE, eeUTF16BE, eeUTF32LE, eeUTF32BE);

  //Algorithm to detect line beside if is Windows,Linux,Max or mix its
  TCoords = class
  private
    count10, count13: Cardinal;
  public
    P: TPoint;
    constructor Create;
    procedure Update(ch: Cardinal);
  end;

  TDataSource = (dsStream, dsString);

  TCharQueue = class
  private
    FStream: TStream;
    FString : string;
    FBuffer: PByteArray;
    FBufSize: Cardinal;
    FEncoding: TEncodingEnum;
    FTail: Cardinal;
    FPeekPtr: Cardinal;
    FSmallQueue: TIntQueue;
    FDataSource: TDataSource;
    procedure PossiblyFillBuffer();
  protected
    function Peek(Index: integer): integer;
  public
    PeekCoords: TCoords;
    procedure DetectEncoding(ProposedEncoding: TEncodingEnum = eeUTF8);
    constructor Create(const AStream: TStream; ABufSize: Cardinal;
      ProposedEncoding: TEncodingEnum = eeUTF8); overload;
    constructor Create(const AString: string); overload;
    destructor Destroy; override;
    function Pop(PCanPop: PBoolean=nil): integer;
    property Encoding: TEncodingEnum read FEncoding;
    property PeekItems[index: integer]: integer read Peek; default;
  end;

const
  UTF8BOM: array[0..2] of Byte = ($EF, $BB, $BF);
  UTF16BOMLE: array[0..1] of Byte = ($FF, $FE);
  UTF16BOMBE: array[0..1] of Byte = ($FE, $FF);
  UTF32BOMLE: array[0..3] of Byte = ($FF, $FE, $00, $00);
  UTF32BOMBE: array[0..3] of Byte = ($00, $00, $FE, $FF);

implementation
uses
  Math;

function BSwap16(n: word):word;assembler;
asm
  ror ax,8
end;

function BSwap32(n: Longword):Longword;assembler;
asm
  bswap eax
end;

{ Returns the index of most significant set bit; for 0 return 0 }
function FindMostSignSetBit(N: Cardinal): Integer;
asm { On entry: eax = ACardinal }
  bsr eax, eax
end;

function GrowCapacity(ACapacity: Cardinal): Cardinal;
var
  PowerOfTwo, Delta: Cardinal;
begin
  PowerOfTwo := 1 shl FindMostSignSetBit(ACapacity);
  if PowerOfTwo >= 64 then
      Delta := PowerOfTwo div 4
  else if PowerOfTwo >= 8 then
      Delta := PowerOfTwo div 2
  else
      Delta := 4;
  result := ACapacity + Delta;
end;

function computeShrinkTrigger(oldCapacity: Cardinal): Integer;
var
  PowerOfTwo: Cardinal;
  Delta: Cardinal;
begin
  if oldCapacity >= 96 then
  begin
    PowerOfTwo := 1 shl FindMostSignSetBit(oldCapacity);
    if oldCapacity >= PowerOfTwo div 2 * 3 then
        Delta := PowerOfTwo div 2
    else if oldCapacity >= PowerOfTwo div 4 * 5 then
        Delta := PowerOfTwo div 8 * 3
    else
        Delta := PowerOfTwo div 4;
  end
  else if oldCapacity >= 80 then
      Delta := 32
  else if oldCapacity >= 16 then
      Delta := oldCapacity div 2
  else
      Delta := 8;
  if Delta > oldCapacity then
    result := 0
  else
      result := oldCapacity - Delta + 1;
end;

{ TIntStack }

procedure TIntStack.Assign(SrcList: TIntStack);
begin
  ReallocMem(FList, SrcList.FCapacity * sizeof(integer));
  Move(SrcList.FList[0], FList[0], SrcList.FCapacity * sizeof(integer));
  FCapacity := SrcList.FCapacity;
  FTop := SrcList.FTop;
  FShrinkTrigger := SrcList.FShrinkTrigger;
end;

function TIntStack.AtLeast(ACount: Integer): Boolean;
begin
  result := Count >= ACount;
end;

procedure TIntStack.Clear;
begin
  FreeMem(FList);
  FList := nil;
  FCapacity := 0;
  FShrinkTrigger := 0;
  FTop := 0;
end;

destructor TIntStack.Destroy;
begin
  Clear;
  inherited;
end;

procedure TIntStack.Grow;
begin
  FCapacity := GrowCapacity(FCapacity);
  ReallocMem(FList, FCapacity * sizeof(integer));
  FShrinkTrigger := computeShrinkTrigger(FCapacity);
end;

procedure TIntStack.Shrink();
begin
  FCapacity := GrowCapacity(FTop - 1);
  ReallocMem(FList, FCapacity * sizeof(integer));
end;

procedure TIntStack.Push(AItem: integer);
begin
  if FTop >= FCapacity then
      Grow();
  FList[FTop] := AItem;
  Inc(FTop);
end;

function TIntStack.GetCount: Integer;
begin
  result := FTop;
end;

function TIntStack.Pop(PCanPop: PBoolean=nil): integer;
begin
  if FTop > 0 then
  begin
    result := FList[FTop - 1];
    Dec(FTop);
    if (FTop>0)and (FTop <= FShrinkTrigger) then
    begin
      Shrink();
      FShrinkTrigger := computeShrinkTrigger(FCapacity);
    end;
    if PCanPop<>nil then PCanPop^:=true;
  end
  else
  begin
    result := 0;
    if PCanPop<>nil then
      PCanPop^:=false
    else
      raise Exception.Create('Pop from empty stack');
  end;
end;

function TIntStack.IsEmpty: Boolean;
begin
  result := FTop = 0;
end;

function TIntStack.Bottom(fromBottom: Cardinal): integer;
begin
  if (fromBottom < FTop) and (fromBottom > 0) then
      result := FList[fromBottom]
  else
      raise Exception.CreateFmt('Bad stack position=%, where Top=%d',
      [fromBottom, Top]);
end;

function TIntStack.Top(fromTop: Cardinal): integer;
begin
  if FTop - fromTop > 0 then
  begin
    result := FList[FTop - 1 - fromTop];
  end
  else
      raise Exception.CreateFmt('Bad stack position=%, where Top=%d',
      [fromTop, Top]);
end;

{ TIntQueue }

procedure TIntQueue.Assign(SrcList: TIntQueue);
begin
  ReallocMem(FList, SrcList.FCapacity * sizeof(integer));
  Move(SrcList.FList[0], FList[0], SrcList.FCapacity * sizeof(integer));
  FCapacity := SrcList.FCapacity;
  FHead := SrcList.FHead;
  FTail := SrcList.FTail;
  FShrinkTrigger := SrcList.FShrinkTrigger;
end;

function TIntQueue.AtLeast(ACount: integer): Boolean;
begin
  result := Count >= ACount;
end;

procedure TIntQueue.Clear;
var
  i: integer;
begin
  if FFreeObjects then
  begin
    if FHead < FTail then
    begin
      for i:=FHead to FTail-1 do
        TObject(FList[i]).Free
     end else
     begin
       for i:=FHead to FCapacity-1 do
         TObject(FList[i]).Free;
       for i:=0 to FTail-1 do
         TObject(FList[i]).Free
     end;
  end;
  FreeMem(FList);
  FList := nil;
  FCapacity := 0;
  FShrinkTrigger := 0;
  FHead := 0;
  FTail := 0;
end;

destructor TIntQueue.Destroy;
begin
  Clear;
  inherited;
end;

function TIntQueue.GetCount: integer;
begin
  if FTail >= FHead then
      result := FTail - FHead
  else
      result := FCapacity - (FHead - FTail);
end;

function TIntQueue.IsEmpty: Boolean;
begin
  result := FHead = FTail;
end;

procedure TIntQueue.Grow;
var
  FCapacityOld: Cardinal;
  Delta: integer;
begin
  FCapacityOld := FCapacity;
  FCapacity := GrowCapacity(FCapacity);
  Delta := FCapacity - FCapacityOld;
  Assert(Delta > 0);
  ReallocMem(FList, FCapacity * sizeof(integer));
  if FTail < FHead then
  begin
    inc(FHead, Delta);
    Move(FList^[FHead - Delta], FList^[FHead], (FCapacity - FHead) *
      sizeof(integer));
  end;
  FShrinkTrigger := computeShrinkTrigger(FCapacity);
end;

procedure TIntQueue.Shrink;
var
  oldCapacity, Delta: integer;
begin
  oldCapacity := FCapacity;
  FCapacity := GrowCapacity(Count - 1);
  if FTail < FHead then
  begin
    Move(FList^[FHead], FList^[FTail + 1], (oldCapacity - FHead) *
      sizeof(integer));
    FHead := FTail + 1;
  end
  else
  begin
    // entire Queue go to left
    Delta := FHead;
    FHead := 0;
    dec(FTail, Delta);
    Move(FList^[Delta], FList^[0], FTail * sizeof(integer));
  end;
  Assert(FCapacity<oldCapacity);
  ReallocMem(FList, FCapacity * sizeof(integer));
  FShrinkTrigger := computeShrinkTrigger(FCapacity);
end;

function TIntQueue.Pop(PCanPop: PBoolean=nil): integer;
begin
  if FHead <> FTail then
  begin
    result := FList[FHead];
    inc(FHead);
    Assert(FHead <= FCapacity);
    if FHead = FCapacity then
    begin
       Assert(FTail <= FCapacity);
       if FTail = FCapacity then
         FTail := 0;
       FHead := 0;
    end;
    if (FHead <> FTail) and (Count <= FShrinkTrigger) then
        Shrink();
    if PCanPop<>nil then PCanPop^:=true;
  end
  else
  begin
    result := 0;
    if PCanPop<>nil then
      PCanPop^:=false
    else
      raise Exception.Create('Pop from empty stack');
  end;
end;

procedure TIntQueue.PopAndFree(PCanPop: PBoolean=nil);
var
  obj: TObject;
begin
  if not FFreeObjects then
    raise Exception.Create('PopAndFree have sense only with FFreeObjects!');
  obj := TObject(Pop(PCanPop));
  if (PCanPop=nil)or(PCanPop^=true) then
    obj.Free;
end;

function TIntQueue.Push(AItem: integer): PInteger;
begin
  { Count+1 because we don't allow situation where entire buffer is full and
    FHead=FTail which is indistinguishable from situation "empty queue" }
  if Count + 1 >= FCapacity then
      Grow();
  if FTail >= FCapacity then
      FTail := 0;
  FList[FTail] := AItem;
  result := @FList[FTail];
  inc(FTail);
end;

function TIntQueue.Head(fromHead: Cardinal): integer;
begin
  result := HeadRef(fromHead)^
end;

function TIntQueue.HeadRef(fromHead: Cardinal): PInteger;
var
  n: integer;
begin
  if FHead <> FTail then
  begin
    n := FHead + fromHead;
    if FHead < FTail then
    begin
      if n >= FTail then
          raise Exception.Create('Get Head(fromHead) out of range');
    end
    else
    begin
      if n >= FCapacity then
      begin
        dec(n, FCapacity);
        if n >= FTail then
            raise Exception.Create('Get Head(fromHead) out of range');
      end;
    end;
    result := @(FList^[n]);
  end
  else
      raise Exception.Create('Get Head() from empty stack');
end;

function TIntQueue.Tail(fromTail: Cardinal): integer;
begin
  result := TailRef(fromTail)^
end;

function TIntQueue.TailRef(fromTail: Cardinal): PInteger;
var
  n: integer;
begin
  if FHead <> FTail then
  begin
    n := integer(FTail) - integer(fromTail) - 1;
    if FHead < FTail then
    begin
      if n < FHead then
          raise Exception.Create('Get Tail(fromTail) out of range');
    end
    else
    begin
      if n < 0 then
      begin
          inc(n, FCapacity);
          if n < FHead then
            raise Exception.Create('Get Tail(fromTail) out of range');
      end;
    end;
    result := @(FList^[n]);
  end
  else
      raise Exception.Create('Get Tail() from empty stack');
end;

constructor TIntQueue.Create(bFreeObjects: Boolean);
begin
  FFreeObjects := bFreeObjects;
end;

{ TCharQueue }
procedure TCharQueue.DetectEncoding(ProposedEncoding: TEncodingEnum = eeUTF8);
begin
  if FTail < 4 then
      FEncoding := ProposedEncoding
  else
    if (FBuffer^[0] = UTF8BOM[0]) and (FBuffer^[1] = UTF8BOM[1]) and
    (FBuffer^[2] = UTF8BOM[2])
  then
  begin
    FEncoding := eeUTF8;
    FPeekPtr := 3;
  end
  else
    if (FBuffer^[0] = UTF16BOMLE[0]) and (FBuffer^[1] = UTF16BOMLE[1])
  then
  begin
    FEncoding := eeUTF16LE;
    FPeekPtr := 2;
  end
  else
    if (FBuffer^[0] = UTF16BOMBE[0]) and (FBuffer^[1] = UTF16BOMBE[1])
  then
  begin
    FEncoding := eeUTF16BE;
    FPeekPtr := 2;
  end
  else
    if (FBuffer^[0] = UTF32BOMLE[0]) and (FBuffer^[1] = UTF32BOMLE[1])
    and (FBuffer^[2] = UTF32BOMLE[2]) and (FBuffer^[3] = UTF32BOMLE[3])
  then
  begin
    FEncoding := eeUTF32LE;
    FPeekPtr := 4;
  end
  else
    if (FBuffer^[0] = UTF32BOMBE[0]) and (FBuffer^[1] = UTF32BOMBE[1])
    and (FBuffer^[2] = UTF32BOMBE[2]) and (FBuffer^[3] = UTF32BOMBE[3])
  then
  begin
    FEncoding := eeUTF32BE;
    FPeekPtr := 4;
  end
  else
      FEncoding := ProposedEncoding;
end;

constructor TCharQueue.Create(const AStream: TStream; ABufSize: Cardinal;
  ProposedEncoding: TEncodingEnum = eeUTF8);
begin
  FDataSource := dsStream;
  ABufSize:=Max(ABufSize, 4); //TCharQueue buffer must have at least 4 bytes for BOM detection
  FStream := AStream;
  FBufSize := ABufSize;
  GetMem(FBuffer, FBufSize);
  FTail := FStream.Read(FBuffer^, FBufSize);
  DetectEncoding;
  FSmallQueue := TIntQueue.Create(false);
  PeekCoords := TCoords.Create;
end;

constructor TCharQueue.Create(const AString: string);
begin
  FDataSource := dsString;
  FString := AString;
  FBufSize := Length(FString)*sizeof(Char);
  FBuffer := PByteArray(PChar(FString));
  FTail := FBufSize;
  if sizeof(Char)=1 then FEncoding := eeUTF8
  else if sizeof(Char)=2 then FEncoding := eeUTF16LE
  else raise Exception.Create('error char size');
  FSmallQueue := TIntQueue.Create(false);
  PeekCoords := TCoords.Create;
end;

destructor TCharQueue.Destroy;
begin
  PeekCoords.Free;
  FSmallQueue.Free;
  if FDataSource<>dsString then FreeMem(FBuffer);
  inherited;
end;

// returns true if EOF
procedure TCharQueue.PossiblyFillBuffer;
begin
  if FDataSource=dsString then exit;
  if FPeekPtr < FTail then exit
  else
  begin
    FTail := FStream.Read(FBuffer^, FBufSize);
    FPeekPtr := 0;
    if FTail = 0 then raise EndOfStreamException.Create('');
  end;
end;

function TCharQueue.Peek(Index: integer): integer;
var
  b0, b1, b2, b3: Byte;
  surrogate1, surrogate2: Word;
begin
  if Index < FSmallQueue.Count then
  begin
    result := FSmallQueue.Head(Index);
    exit;
  end else
    if Index > FSmallQueue.Count then
      raise Exception.Create('Peek can''t jump over character');

  Assert(Index = FSmallQueue.Count);
  if FPeekPtr + 3 < FTail then
  begin // speedup for most cases, not boundary case
    case FEncoding of
      eeUTF8:
        begin
          b0 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          if b0 and $80 = 0 then
              result := b0
          else if b0 and $20 = 0 then
          begin
            b1 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            result := (b1 and 63) or (Cardinal(b0 and 31) shl 6);
          end
          else if b0 and $10 = 0 then
          begin
            b1 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            b2 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            result := (b2 and 63) or (Cardinal(b1 and 63) shl 6) or
              (Cardinal(b0 and 15) shl 12);
          end
          else
          begin
            b1 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            b2 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            b3 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            result := (b3 and 63) or (Cardinal(b2 and 63) shl 6)
              or (Cardinal(b1 and 63) shl 12) or (Cardinal(b0 and 7) shl 18);
          end;
        end;
      eeUTF16LE:
        begin
          surrogate1 := PWord(@FBuffer[FPeekPtr])^;
          inc(FPeekPtr,2);
          if (surrogate1 >= $D800) and (surrogate1 <= $DBFF) then
          begin
            surrogate2 := PWord(@FBuffer[FPeekPtr])^;
            inc(FPeekPtr,2);
            result := (surrogate1 - $D800) shl 10 +
              (surrogate2 - $DC00) + $10000;
          end
          else
              result := surrogate1;
        end;
      eeUTF16BE:
        begin
          surrogate1 := BSwap16(PWord(@FBuffer[FPeekPtr])^);
          inc(FPeekPtr,2);
          if (surrogate1 >= $D800) and (surrogate1 <= $DBFF) then
          begin
            surrogate2 := BSwap16(PWord(@FBuffer[FPeekPtr])^);
            inc(FPeekPtr,2);
            result := (surrogate1 - $D800) shl 10 +
              (surrogate2 - $DC00) + $10000;
          end
          else
              result := surrogate1;
        end;
      eeUTF32LE:
        begin
          result:=PDWord(@FBuffer[FPeekPtr])^;
          inc(FPeekPtr,4);
        end;
      eeUTF32BE:
        begin
          result:=BSwap32(PDWord(@FBuffer[FPeekPtr])^);
          inc(FPeekPtr,4);
        end;
    end;
  end
  else
  begin
    result := -1; // Default: end of file
    case FEncoding of
      eeUTF8:
        begin
          PossiblyFillBuffer();
          b0 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          if b0 and $80 = 0 then
              result := b0
          else if b0 and $20 = 0 then
          begin
            PossiblyFillBuffer();
            b1 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            result := (b1 and 63) or (Cardinal(b0 and 31) shl 6);
          end
          else if b0 and $10 = 0 then
          begin
            PossiblyFillBuffer();
            b1 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            PossiblyFillBuffer();
            b2 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            result := (b2 and 63) or (Cardinal(b1 and 63) shl 6) or
              (Cardinal(b0 and 15) shl 12);
          end
          else
          begin
            PossiblyFillBuffer();
            b1 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            PossiblyFillBuffer();
            b2 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            PossiblyFillBuffer();
            b3 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            result := (b3 and 63) or (Cardinal(b2 and 63) shl 6)
              or (Cardinal(b1 and 63) shl 12) or (Cardinal(b0 and 7) shl 18);
          end;
        end;
      eeUTF16LE:
        begin
          PossiblyFillBuffer();
          b0 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b1 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          surrogate1 := (b1 shl 8) or b0;
          if (surrogate1 >= $D800) and (surrogate1 <= $DBFF) then
          begin
            PossiblyFillBuffer();
            b2 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            PossiblyFillBuffer();
            b3 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            surrogate2 := (b3 shl 8) or b2;
            result := (surrogate1 - $D800) shl 10 +
              (surrogate2 - $DC00) + $10000;
          end
          else
              result := surrogate1;
        end;
      eeUTF16BE:
        begin
          PossiblyFillBuffer();
          b1 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b0 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          surrogate1 := (b1 shl 8) or b0;
          if (surrogate1 >= $D800) and (surrogate1 <= $DBFF) then
          begin
            PossiblyFillBuffer();
            b3 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            PossiblyFillBuffer();
            b2 := FBuffer[FPeekPtr];
            inc(FPeekPtr);
            surrogate2 := (b3 shl 8) or b2;
            result := (surrogate1 - $D800) shl 10 +
              (surrogate2 - $DC00) + $10000;
          end
          else
              result := surrogate1;
        end;
      eeUTF32LE:
        begin
          PossiblyFillBuffer();
          b0 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b1 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b2 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b3 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          with LongRec(result) do
          begin
            Bytes[0]:=b0;
            Bytes[1]:=b1;
            Bytes[2]:=b2;
            Bytes[3]:=b3;
          end;
        end;
      eeUTF32BE:
        begin
          PossiblyFillBuffer();
          b3 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b2 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b1 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          PossiblyFillBuffer();
          b0 := FBuffer[FPeekPtr];
          inc(FPeekPtr);
          with LongRec(result) do
          begin
            Bytes[0]:=b0;
            Bytes[1]:=b1;
            Bytes[2]:=b2;
            Bytes[3]:=b3;
          end;
        end;
    end;
  end;
  Assert(Index = FSmallQueue.Count);
  FSmallQueue.Push(result);
  PeekCoords.Update(result);
end;

function TCharQueue.Pop(PCanPop: PBoolean=nil): integer;
var
  CanPop: Boolean;
begin
  if FSmallQueue.IsEmpty then
     Peek(0);
  //second ask IsEmpty because Peek(0) move from buffer to aux queue
  if FSmallQueue.IsEmpty then
  begin
    result := -1;
    if PCanPop<>nil then
      PCanPop^:=false
    else
      raise Exception.Create('Pop from empty char queue');
  end else
  begin
    result := FSmallQueue.Pop;
    if PCanPop<>nil then PCanPop^:=true;
  end;
end;

{ TCoords }

constructor TCoords.Create;
begin
  P.X := 0;
  P.Y := 1;
end;

procedure TCoords.Update(ch: Cardinal);
begin
  if (ch = 13) or (ch = 10) then
  begin
    if  ch = 13 then inc(count13);
    if  ch = 10 then inc(count10);
    inc(P.X);
  end
  else
  begin
    if (count10>0) or (count13>0) then
    begin
      inc(P.Y, Max(count10, count13));
      count10:=0;
      count13:=0;
      P.X := 0;
    end;
    inc(P.X);
  end;
end;

{ TThreadQueue }

procedure TThreadQueue.Assign(SrcList: TThreadQueue);
begin
  EnterCriticalSection(FLock);
  try
    FQueue.Assign(SrcList.FQueue);
  finally
    LeaveCriticalSection(FLock);
  end;
end;

function TThreadQueue.AtLeast(ACount: integer): Boolean;
begin
  EnterCriticalSection(FLock);
  try
    result := FQueue.AtLeast(ACount);
  finally
    LeaveCriticalSection(FLock);
  end;
end;

procedure TThreadQueue.Clear;
begin
  EnterCriticalSection(FLock);
  try
    FQueue.Clear;
  finally
    LeaveCriticalSection(FLock);
  end;
end;

constructor TThreadQueue.Create(bFreeObjects: Boolean);
begin
  InitializeCriticalSection(FLock);
  FQueue := TIntQueue.Create(bFreeObjects);
end;

destructor TThreadQueue.Destroy;
begin
  EnterCriticalSection(FLock);
  try
    FQueue.Free;
    inherited Destroy;
  finally
    LeaveCriticalSection(FLock);
    DeleteCriticalSection(FLock);
  end;
end;

function TThreadQueue.GetCount: integer;
begin
  EnterCriticalSection(FLock);
  try
    result := FQueue.GetCount();
  finally
    LeaveCriticalSection(FLock);
  end;
end;

function TThreadQueue.Head(fromHead: Cardinal): integer;
begin
  EnterCriticalSection(FLock);
  try
    result:=FQueue.Head(fromHead);
  finally
    LeaveCriticalSection(FLock);
  end;
end;

function TThreadQueue.Pop(PCanPop: PBoolean): integer;
begin
  EnterCriticalSection(FLock);
  try
    result:=FQueue.Pop(PCanPop);
  finally
    LeaveCriticalSection(FLock);
  end;
end;

procedure TThreadQueue.PopAndFree(PCanPop: PBoolean);
begin
  EnterCriticalSection(FLock);
  try
    FQueue.PopAndFree(PCanPop);
  finally
    LeaveCriticalSection(FLock);
  end;
end;

procedure TThreadQueue.Push(AItem: integer);
begin
  EnterCriticalSection(FLock);
  try
    FQueue.Push(AItem);
  finally
    LeaveCriticalSection(FLock);
  end;
end;

function TThreadQueue.Tail(fromTail: Cardinal): integer;
begin
  EnterCriticalSection(FLock);
  try
    result:=FQueue.Tail(fromTail);
  finally
    LeaveCriticalSection(FLock);
  end;
end;

{ TInterfaceQueue }

function TInterfaceQueue.AtLeast(ACount: integer): Boolean;
begin
  result := FQueue.AtLeast(ACount);
end;

procedure TInterfaceQueue.Clear;
begin
  ClearIntf;
  FQueue.Clear;
end;

procedure TInterfaceQueue.ClearIntf;
var
  i: integer;
begin
  for i:=0 to FQueue.GetCount()-1 do
    IInterface(FQueue.HeadRef(i)^) := nil;
end;

constructor TInterfaceQueue.Create();
begin
  FQueue := TIntQueue.Create(false);
end;

destructor TInterfaceQueue.Destroy;
begin
  ClearIntf;
  FQueue.Free;
  inherited;
end;

function TInterfaceQueue.GetCount: integer;
begin
  result := FQueue.GetCount();
end;

function TInterfaceQueue.IsEmpty: Boolean;
begin
  result:=FQueue.IsEmpty;
end;

function TInterfaceQueue.Head(fromHead: Cardinal): IInterface;
begin
  result:=IInterface(FQueue.HeadRef(fromHead)^);
end;

function TInterfaceQueue.Tail(fromTail: Cardinal): IInterface;
begin
  result:=IInterface(FQueue.TailRef(fromTail)^);
end;

function TInterfaceQueue.Pop(PCanPop: PBoolean): IInterface;
var
  p: PInteger;
begin
  if PCanPop<>nil then
  begin
    PCanPop^:=not FQueue.IsEmpty;
    if PCanPop^=false then
    begin
      result := nil;
      exit;
    end;
  end;
  p := FQueue.HeadRef(0);
  result:=IInterface(p^);
  IInterface(p^) := nil;
  FQueue.Pop(nil);
end;

procedure TInterfaceQueue.PopAndFree(PCanPop: PBoolean=nil);
var
  p: PInteger;
begin
  if PCanPop<>nil then
  begin
    PCanPop^:=not FQueue.IsEmpty;
    if PCanPop^=false then exit;
  end;
  p := FQueue.HeadRef(0);
  IInterface(p^) := nil;
  FQueue.Pop(nil);
end;


procedure TInterfaceQueue.Push(AItem: IInterface);
begin
  IInterface(FQueue.Push(0)^) := AItem;
end;

end.
