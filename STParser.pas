// see ISC_license.txt
{$I genLL.inc}
unit STParser;

interface

uses
  Runtime, Trees, STLexer;

const
  PROD_TEMPLATE   = 1;

type
  TSTParser = class(TParser)
  private
  public
    {function syn_program(): TTree;
    function syn_block(): TTree;}

  end;

implementation

uses
  Tokens;

function TSpokolParser.syn_template: TTree;
var
  LA1: integer;
begin
  result := TTree.Create('template');
  Match(LEX_KEY_PROGRAM);
  Match(LEX_IDENT);
  if LA(1) = LEX_KEY_VAR then
      result.addChild(syn_var_seq);
  if LA(1) = LEX_KEY_PROCEDURE then
      result.addChild(syn_proc_seq);
  result.addChild(syn_instr_block);
end;
{

function TSpokolParser.syn_block: TTree;
var
  t: IToken;
begin
  result := TTree.Create('block');
  if LA(1) = LEX_KEY_VAR then
      result.addChild(syn_var_seq);
  t := Match(LEX_KEY_BEGIN);
  result.addToken(t);
  result.addChild(syn_instr_seq);
  t := Match(LEX_KEY_END);
  result.addToken(t);
end;
 }

end.
