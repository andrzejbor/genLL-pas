// see ISC_license.txt
{$I genLL.inc}
program E;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Classes,
  SysUtils,
  Runtime in 'Runtime.pas',
  ELexer in 'ELexer.pas',
  EParser in 'EParser.pas',
  nQueues in 'nQueues.pas';

var
  Stream: TFileStream;
  Lex: TELexer;
  Parser: TEParser;
begin
  Stream := TFileStream.Create('badE.in', fmOpenRead);
  Lex := TELexer.Create(Stream);
  Parser := TEParser.Create(Lex);
  Parser.prog;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
