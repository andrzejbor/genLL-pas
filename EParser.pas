// see ISC_license.txt
{$I genLL.inc}
unit EParser;

interface

uses
  Runtime, ELexer;

type
  TEParser = class(TParser)
  public
    procedure prog;
    procedure stat;
    procedure expr;
    procedure multExpr;
    procedure atom;
  end;

implementation

{ TGramParser }
procedure TEParser.prog();
var
  LA1: integer;
  cnt: integer;
begin
  cnt := 0;
  repeat
    LA1 := LA(1);
    if LA1 = LEX_EOF then
        break;
    if (LA1 <> LEX_ID) and (LA1 <> LEX_4) and (LA1 <> LEX_INT) then
    begin
      ErrorP('error in prog');
      if LA(1) = LEX_10 then
          RecoveryInsert(LEX_ID)
      else
          Consume();
    end;
    stat();
    inc(cnt);
  until false;
end;

procedure TEParser.stat;
var
  LA1: integer;
begin
  LA1 := LA(1);
  if (LA1 = LEX_INT) or (LA1 = LEX_4) then
  begin
    expr();
    Match(LEX_9);
  end
  else if LA1 = LEX_ID then
  begin
    Consume;
    Match(LEX_10);
    expr();
    Match(LEX_9);
  end;
end;

procedure TEParser.expr;
var
  LA1: integer;
begin
  multExpr();
  repeat
    LA1 := LA(1);
    if (LA1 = LEX_7) or (LA1 = LEX_8) then
        Consume
    else
        break;
  until false;
end;

procedure TEParser.multExpr;
var
  LA1: integer;
begin
  atom();
  repeat
    LA1 := LA(1);
    if LA1 = LEX_6 then
        Consume
    else
        break;
  until false;
end;

procedure TEParser.atom;
var
  LA1: integer;
begin
  LA1 := LA(1);
  if LA1 = LEX_INT then
      Consume
  else if LA1 = LEX_4 then
  begin
    Consume;
    expr;
    Match(LEX_5);
  end
  else
  begin
    ErrorP('eror in atom');
  end;
end;

end.
