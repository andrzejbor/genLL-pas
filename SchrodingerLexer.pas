// see ISC_license.txt
{$I genLL.inc}
unit SchrodingerLexer;

interface

uses
  SysUtils, Runtime;

const
  MaskHi = 1 shl 31; //fro Schrodinger comparison

  LEX_ID = MaskHi;
  LEX_IF = MaskHi + 1;
  LEX_THEN = MaskHi + 2;
  LEX_ASSIGNMENT = 1;
  LEX_EQUALS = 2;
  LEX_NOT_EQUALS = 3;
  LEX_SEMI = 4;
  LEX_INT = 5;
  LEX_WS = 6;
  LEX_COMMENT = 7;

type
  TSchrodingerLexer = class(TLexer)
  protected
    procedure Tokens(); override;
    function GetTokenName(tokenType: integer): string; override;
    function fENDL(): boolean;
    function fLETTER(): boolean;
    function fDIGIT(): boolean;
    procedure mWS();
    procedure mCOMMENT();
    procedure mID();
    procedure mINT();
    procedure mIF();
    procedure mTHEN();

  end;

implementation
uses
  Tokens;

procedure TSchrodingerLexer.Tokens;
var
  LA1: integer;
  lextype: integer;
begin
  LA1 := LA(1);
  if LA1 = LEX_EOF then
  begin
    ApproveTokenBuf(LEX_EOF, ceDefault);
    exit;
  end;
  case LA1 of
    9 .. 10, 13, ord(' '): mWS();
    ord('A') .. ord('Z'), ord('a') .. ord('z'), ord('_'):
      begin
        if LA1 = ord('i') then
        begin
          if CompareLatin('if') then
              mIF()
          else
              mID();
        end
        else
          if LA1 = ord('t') then
        begin
          if CompareLatin('then') then
              mTHEN()
          else
              mID();
        end
        else
          mID();
      end;
    ord('0') .. ord('9'): mINT();
    ord('/'): if LA(2) = ord('/') then
          mCOMMENT()
      else
          raise ENoViableAltException.Create('');
    ord(';'), ord('='), ord('!'):
      begin
        case LA1 of
          ord(';'): lextype := LEX_SEMI;
          ord('='): if LA(2) = ord('=') then
            begin
                lextype := LEX_EQUALS;
                Consume();
            end
            else
              lextype := LEX_ASSIGNMENT;
          ord('!'): if LA(2) = ord('=') then
            begin
              lextype := LEX_NOT_EQUALS;
              Consume();
            end
            else
              raise ENoViableAltException.Create('');
        end;
        Consume;
        ApproveTokenBuf(lextype, ceDefault);
      end;
  end;
end;

function TSchrodingerLexer.GetTokenName(tokenType: integer): string;
begin
  case tokenType of
    LEX_ID: result := 'Ident';
    LEX_IF: result := 'if';
    LEX_THEN: result := 'then';
    LEX_ASSIGNMENT: result := '=';
    LEX_EQUALS: result := '==';
    LEX_NOT_EQUALS: result := '!=';
    LEX_SEMI: result := ';';
    LEX_INT: result := 'int';
    LEX_WS: result := 'whitespace';
    LEX_COMMENT: result := 'comment';
  end;
end;

function TSchrodingerLexer.fENDL(): boolean;
begin
  result := (LA(1) = 13) and (LA(2) = 10) or (LA(1) = 10);
end;

function TSchrodingerLexer.fLETTER(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('A')) and (ch <= ord('Z'))
    or (ch >= ord('a')) and (ch <= ord('z'))
end;

function TSchrodingerLexer.fDIGIT(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('0')) and (ch <= ord('9'));
end;

procedure TSchrodingerLexer.mWS();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 = 9) or fENDL() or (LA1 = ord(' '))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_WS, ceHidden);
end;

procedure TSchrodingerLexer.mCOMMENT;
var
  LA1: integer;
begin
  MatchLatin('\\');
  repeat
    LA1 := LA(1);
    if (LA1 <> -1) and not fENDL() then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_COMMENT, ceHidden);
end;

procedure TSchrodingerLexer.mID();
var
  LA1: integer;
begin
  if fLETTER() or (LA(1) = ord('_')) then
      Consume;
  repeat
    LA1 := LA(1);
    if fLETTER() or (LA(1) = ord('_')) or fDIGIT() then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_ID, ceDefault);
end;

procedure TSchrodingerLexer.mINT();
var
  LA1: integer;
begin
  if LA(1) = ord('0') then
      Consume()
  else
  begin
    Consume(); // 1..9
    while fDIGIT() do
        Consume();
  end;
  ApproveTokenBuf(LEX_INT, ceDefault);
end;

procedure TSchrodingerLexer.mIF;
begin
  MatchLatin('if');
  ApproveTokenBuf(LEX_IF, ceDefault);
end;

procedure TSchrodingerLexer.mTHEN;
begin
  MatchLatin('then');
  ApproveTokenBuf(LEX_THEN, ceDefault);
end;

end.
