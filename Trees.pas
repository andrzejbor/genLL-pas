unit Trees;

interface

uses
  Classes, Tokens;

type
  TTree = class
  private
    FChildren: TList;
    FParent: TTree; //if null it is root
    FToken: IToken;
    fDesc: AnsiString;
  public
    constructor Create(token: IToken); overload;
    constructor Create(desc: AnsiString); overload;
    destructor Destroy; override;
    function getChild(n: integer): TTree;
    function getChildCount(): integer;
    function getParent(): TTree;
    procedure addChild(t: TTree);
    procedure addToken(token: IToken);
    procedure setMainToken(token: IToken);
    procedure deleteChild(n: integer);
    procedure print(depth: integer);
  end;

implementation

{ TTree }

procedure TTree.addToken(token: IToken);
begin
  addChild(TTree.Create(token));
end;

procedure TTree.setMainToken(token: IToken);
begin
  FToken := token;
end;

procedure TTree.addChild(t: TTree);
begin
  if FChildren=nil then
    FChildren:=TList.Create;
  FChildren.Add(t);
  t.FParent := self;
end;

constructor TTree.Create(token: IToken);
begin
  FToken := token;
end;

constructor TTree.Create(desc: AnsiString);
begin
  FDesc := desc;
end;

procedure TTree.deleteChild(n: integer);
begin
  FChildren.Delete(n);
end;

destructor TTree.Destroy;
var
  i: integer;
begin
  if FChildren<>nil then
  for i:=0 to FChildren.Count-1 do
    TObject(FChildren[i]).Free;
  FChildren.Free;
  inherited;
end;

function TTree.getChild(n: integer): TTree;
begin
  result:=FChildren[n];
end;

function TTree.getChildCount: integer;
begin
  if FChildren=nil then
    result:=0
  else
    result:=FChildren.Count;
end;

function TTree.getParent: TTree;
begin
  result := FParent;
end;

procedure TTree.print(depth: integer);
var
  i:integer;
begin
  for i:=0 to depth-1 do write('  ');
  if FToken=nil then
    writeln(fDesc)
  else
    writeln(FToken.getValue);
  if FChildren<>nil then
  for i:=0 to FChildren.Count-1 do
    TTree(FChildren[i]).print(depth+1);
end;

end.
