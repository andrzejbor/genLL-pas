// see ISC_license.txt
{$I genLL.inc}
unit ELexer;

interface

uses
  Sysutils, Runtime;

const
  { token type }
  LEX_ID = 1; // start from 128 ?
  LEX_INT = 2;
  LEX_WS = 3;
  LEX_4 = 4; // '('
  LEX_5 = 5; // ')'
  LEX_6 = 6; // '*'
  LEX_7 = 7; // '+'
  LEX_8 = 8; // '-'
  LEX_9 = 9; // ';'
  LEX_10 = 10; // '='

type
  TELexer = class(TLexer)
  protected
    procedure Tokens(); override;
    function GetTokenName(tokenType: integer): string; override;
    procedure mID();
    procedure mINT();
    procedure mWS();
  end;

implementation
uses
  Tokens;

procedure TELexer.Tokens;
var
  LA1: integer;
  lextype: integer;
begin
  LA1 := LA(1);
  if LA1 = LEX_EOF then
  begin
    ApproveTokenBuf(LEX_EOF, ceDefault);
    exit;
  end;
  case LA1 of
    9 .. 10, 12 .. 13, ord(' '): mWS();
    ord('A') .. ord('Z'), ord('a') .. ord('z'): mID();
    ord('0') .. ord('9'): mINT();
    ord('('), ord(')'), ord('*'), ord('+'),
      ord('-'), ord(';'), ord('='):
      begin
        case LA1 of
          ord('('): lextype := LEX_4;
          ord(')'): lextype := LEX_5;
          ord('*'): lextype := LEX_6;
          ord('+'): lextype := LEX_7;
          ord('-'): lextype := LEX_8;
          ord(';'): lextype := LEX_9;
          ord('='): lextype := LEX_10;
        end;
        Consume;
        ApproveTokenBuf(lextype, ceDefault);
      end;
  else raise ENoViableAltException.Create('');
  end;
end;

procedure TELexer.mWS();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 = 9) or (LA1 = 10) or (LA1 = 12) or (LA1 = 13) or (LA1 = ord(' '))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_WS, ceHidden);
end;

function TELexer.GetTokenName(tokenType: integer): string;
begin
  case tokenType of
    LEX_ID : result := 'Ident';
    LEX_INT : result := 'INT';
    LEX_WS : result := 'white space';
    LEX_4 : result := '(';
    LEX_5 : result := ')';
    LEX_6 : result := '*';
    LEX_7 : result := '+';
    LEX_8 : result := '-';
    LEX_9 : result := ';';
    LEX_10 : result := '=';
  end;
end;

procedure TELexer.mID();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 >= ord('A')) and (LA1 <= ord('Z')) or
      (LA1 >= ord('a')) and (LA1 <= ord('z'))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_ID, ceDefault);
end;

procedure TELexer.mINT();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 >= ord('0')) and (LA1 <= ord('9')) then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_INT, ceDefault);
end;

end.
