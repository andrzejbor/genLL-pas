// generate potientally big file *.cm
unit CMinusMaker;

interface

uses
  Classes;

type
  TCMinusMaker = class
  private
    variables: TStringList;
    functions: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure make_program;
    procedure make_declaration;
    procedure make_function;
    procedure make_stmt(bReturn: Boolean);
    procedure make_returnStmt;
    procedure make_assignmentStmt;
    procedure make_expression;
    procedure make_addSubExpr;
    procedure make_multDivExpr;
    procedure make_negationExpr;
    procedure make_term;
  end;

implementation

uses
  SysUtils;

{ TCMinusMaker }

constructor TCMinusMaker.Create;
begin
  variables := TStringList.Create;
  functions := TStringList.Create;
  randSeed:=0;
end;

destructor TCMinusMaker.Destroy;
begin
  functions.Free;
  variables.Free;
  inherited;
end;

procedure TCMinusMaker.make_declaration;
var
  vartype: integer;
  name: string;
begin
  vartype := random(2);
  repeat
    if vartype = 0 then
      case random(4) of
        0: name := 'i' + IntToStr(random(10000));
        1: name := 'j' + IntToStr(random(10000));
        2: name := 'k' + IntToStr(random(10000));
        3: name := 'n' + IntToStr(random(10000));
      end
    else
        name := 's' + IntToStr(random(10000));
  until variables.IndexOf(name) < 0;
  variables.AddObject(name, TObject(vartype));
  if vartype = 0 then
      write('int ')
  else
      write('string ');
  writeln(name, ';');
end;

procedure TCMinusMaker.make_function;
var
  vartype: integer;
  name: string;
  i: integer;
begin
  vartype := random(2);
  repeat
      name := 'fun' + IntToStr(random(10000));
  until functions.IndexOf(name) < 0;
  functions.AddObject(name, TObject(vartype));
  if vartype = 0 then
      write('int ')
  else
      write('string ');
  writeln(name, '()');
  writeln('{');
  for i := 0 to 29 do
      make_stmt(i = 29);
  writeln('}');
end;

procedure TCMinusMaker.make_program;
var
  i: integer;
begin
  for i := 0 to 29 do
      make_declaration;
  for i := 0 to 19 do
      make_function;
end;

procedure TCMinusMaker.make_returnStmt;
begin
  write('return ');
  make_expression;
  writeln(';');
end;

procedure TCMinusMaker.make_assignmentStmt;
begin
  write(variables[random(variables.Count)]);
  write('=');
  make_expression;
  writeln(';');
end;

procedure TCMinusMaker.make_stmt(bReturn: Boolean);
begin
  if bReturn then
      make_returnStmt
  else
    if random(2) = 0 then
      make_assignmentStmt
  else begin
    make_expression;
    writeln(';');
  end;
end;

procedure TCMinusMaker.make_expression;
begin
  make_addSubExpr;
end;

procedure TCMinusMaker.make_addSubExpr;
begin
  make_multDivExpr;
  if random(2) > 0 then
  begin
    if random(2) > 0 then
      write('+')
    else
      write('-');
    make_multDivExpr;
  end;
end;

procedure TCMinusMaker.make_multDivExpr;
begin
  make_negationExpr;
  if random(2) > 0 then
  begin
    if random(2) > 0 then
      write('*')
    else
      write('/');
    make_negationExpr;
  end;
end;

procedure TCMinusMaker.make_negationExpr;
begin
  if random(2) > 0 then
    write('-');
  make_term;
end;

procedure TCMinusMaker.make_term;
begin
  case random(5) of
    0: writeln(IntToStr(random(1000)));
    1: writeln('"', IntToStr(random(1000)), '"');
    2: writeln(functions[random(functions.Count)], '()');
    3:
      begin
        write('(');
        make_expression;
        writeln(')');
      end;
    4: writeln(variables[random(variables.Count)]);
  end;
end;

end.
