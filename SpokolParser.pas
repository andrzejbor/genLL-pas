// see ISC_license.txt
{$I genLL.inc}
unit SpokolParser;

interface

uses
  Runtime, Trees, SpokolLexer;

const
  PROD_PROGRAM = 1;
  PROD_BLOCK = 2;
  PROD_VAR_SEQ = 3;
  PROD_VAR_DECL = 4;
  PROD_IDENT_LIST = 5;
  PROD_INSTR_SEQ = 6;
  PROD_INSTR_BLOCK = 7;
  PROD_PROC_SEQ = 8;
  PROD_PROC_DECL = 9;
  PROD_PROC_HEAD = 10;
  PROD_INSTR = 11;
  PROD_PRINT = 12;
  PROD_READ = 13;
  PROD_IF = 14;
  PROD_WHILE = 15;
  PROD_BREAK = 16;
  PROD_ASSIGN = 17;
  PROD_PROC_CALL = 18;
  PROD_EXPR = 19;
  PROD_SIMP_EXPR = 20;
  PROD_SIGN = 21;
  PROD_TERM = 22;
  PROD_FACTOR = 23;

type
  TSpokolParser = class(TParser)
  private
  public
    function syn_program(): TTree;
    function syn_block(): TTree;
    function syn_var_seq(): TTree;
    function syn_var_decl(): TTree;
    function syn_ident_list(): TTree;
    function syn_instr_seq(): TTree;
    function syn_instr_block(): TTree;
    function syn_proc_seq(): TTree;
    function syn_proc_decl(): TTree;
    function syn_proc_head(): TTree;
    function syn_instr(): TTree;
    function syn_print(): TTree;
    function syn_read(): TTree;
    function syn_if(): TTree;
    function syn_while(): TTree;
    function syn_break(): TTree;
    function syn_assign(): TTree;
    function syn_proc_call(): TTree;
    function syn_expr(): TTree;
    function syn_simp_expr(): TTree;
    function syn_sign(): TTree;
    function syn_term(): TTree;
    function syn_factor(): TTree;
  end;

implementation

uses
  Tokens;

function TSpokolParser.syn_program: TTree;
var
  LA1: integer;
begin
  result := TTree.Create('program');
  Match(LEX_KEY_PROGRAM);
  Match(LEX_IDENT);
  if LA(1) = LEX_KEY_VAR then
      result.addChild(syn_var_seq);
  if LA(1) = LEX_KEY_PROCEDURE then
      result.addChild(syn_proc_seq);
  result.addChild(syn_instr_block);
end;

function TSpokolParser.syn_block: TTree;
var
  t: IToken;
begin
  result := TTree.Create('block');
  if LA(1) = LEX_KEY_VAR then
      result.addChild(syn_var_seq);
  t := Match(LEX_KEY_BEGIN);
  result.addToken(t);
  result.addChild(syn_instr_seq);
  t := Match(LEX_KEY_END);
  result.addToken(t);
end;

function TSpokolParser.syn_var_seq: TTree;
begin
  result := TTree.Create('var_seq');
  result.addChild(syn_var_decl);
  while LA(1) = LEX_KEY_VAR do
      result.addChild(syn_var_decl);
end;

function TSpokolParser.syn_var_decl: TTree;
var
  t: IToken;
  LA1: integer;
begin
  result := TTree.Create('var_decl');
  Match(LEX_KEY_VAR);
  result.addChild(syn_ident_list);
  Match(LEX_COLON);
  t := LT(1);
  LA1 := t.getType;
  if LA1 in [LEX_KEY_INT, LEX_KEY_REAL, LEX_KEY_STRING, LEX_KEY_BOOL] then
  begin
    result.addToken(t);
    Consume;
  end
  else
      ErrorP('expected type');
end;

function TSpokolParser.syn_ident_list: TTree;
var
  t: IToken;
begin
  result := TTree.Create('ident_list');
  t := Match(LEX_IDENT);
  result.addToken(t);
  while LA(1) = LEX_COMMA do
  begin
    Consume;
    t := Match(LEX_IDENT);
    result.addToken(t);
  end;
end;

function TSpokolParser.syn_instr_seq: TTree;
var
  t: IToken;
  LA1: integer;
begin
  result := TTree.Create('instr_seq');
  t := LT(1);
  LA1 := t.getType;
  while LA(1) in [LEX_KEY_PRINT, LEX_KEY_READ, LEX_KEY_IF,
                  LEX_KEY_WHILE, LEX_KEY_BREAK, LEX_IDENT] do
  begin
    result.addChild(syn_instr);
  end;
end;

function TSpokolParser.syn_instr_block: TTree;
begin
  result := TTree.Create('instr_block');
  Match(LEX_KEY_BEGIN);
  result.addChild(syn_instr_seq);
  Match(LEX_KEY_END);
end;

function TSpokolParser.syn_proc_seq: TTree;
begin
  result := TTree.Create('proc_seq');
  while LA(1) = LEX_KEY_PROCEDURE do
      result.addChild(syn_proc_decl);
end;

function TSpokolParser.syn_proc_decl: TTree;
begin
  result := TTree.Create('proc_decl');
  result.addChild(syn_proc_head);
  result.addChild(syn_block);
end;

function TSpokolParser.syn_proc_head: TTree;
var
  t: IToken;
begin
  result := TTree.Create('proc_head');
  Match(LEX_KEY_PROCEDURE);
  t := Match(LEX_IDENT);
  result.addToken(t);
end;

function TSpokolParser.syn_instr: TTree;
var
  LA1: integer;
begin
  result := TTree.Create('instr');
  LA1 := LA(1);
  case LA1 of
    LEX_KEY_PRINT: result.addChild(syn_print);
    LEX_KEY_READ: result.addChild(syn_read);
    LEX_KEY_IF: result.addChild(syn_if);
    LEX_KEY_WHILE: result.addChild(syn_while);
    LEX_KEY_BREAK: result.addChild(syn_break);
    LEX_IDENT: if LA(2) = LEX_ASSIGN then
          result.addChild(syn_assign)
      else
          result.addChild(syn_proc_call);
  end;
end;

function TSpokolParser.syn_print: TTree;
begin
  result := TTree.Create('print');
  Match(LEX_KEY_PRINT);
  Match(LEX_PARENTHESIS_OPEN);
  result.addChild(syn_expr);
  Match(LEX_PARENTHESIS_CLOSE);
end;

function TSpokolParser.syn_read: TTree;
var
  t: IToken;
begin
  result := TTree.Create('read');
  Match(LEX_KEY_READ);
  Match(LEX_PARENTHESIS_OPEN);
  t := Match(LEX_IDENT);
  result.addToken(t);
  Match(LEX_PARENTHESIS_CLOSE);
end;

function TSpokolParser.syn_if: TTree;
begin
  result := TTree.Create('if');
  Match(LEX_KEY_IF);
  result.addChild(syn_expr);
  Match(LEX_KEY_THEN);
  result.addChild(syn_instr_seq);
  if LA(1) = LEX_KEY_ELSE then
  begin
      Match(LEX_KEY_ELSE);
      result.addChild(syn_instr_seq);
  end;
  Match(LEX_KEY_ENDIF);
end;

function TSpokolParser.syn_while: TTree;
begin
  result := TTree.Create('while');
  Match(LEX_KEY_WHILE);
  result.addChild(syn_expr);
  Match(LEX_KEY_DO);
  result.addChild(syn_instr_seq);
  Match(LEX_KEY_ENDWHILE);
end;

function TSpokolParser.syn_break: TTree;
begin
  result := TTree.Create('break');
  Match(LEX_KEY_BREAK);
end;

function TSpokolParser.syn_assign: TTree;
var
  t: IToken;
begin
  result := TTree.Create('assign');
  t := Match(LEX_IDENT);
  result.addToken(t);
  Match(LEX_ASSIGN);
  result.addChild(syn_expr);
end;

function TSpokolParser.syn_proc_call: TTree;
var
 t: IToken;
begin
  result := TTree.Create('proc_call');
  t:=Match(LEX_IDENT);
  result.setMainToken(t);
end;

function TSpokolParser.syn_expr: TTree;
begin
  result := TTree.Create('expr');
  result.addChild(syn_simp_expr);
  if LA(1) in [LEX_EQUAL, LEX_NOT_EQUAL, LEX_LESS, LEX_GREATER,
    LEX_LESS_EQUAL, LEX_GREATER_EQUAL] then
  begin
    Consume;
    result.addChild(syn_simp_expr);
  end;
end;

function TSpokolParser.syn_simp_expr: TTree;
begin
  result := TTree.Create('simp_expr');
  if LA(1) in [LEX_MINUS_UNARY, LEX_PLUS] then
      result.addChild(syn_sign);
  result.addChild(syn_term);
  while LA(1) in [LEX_PLUS, LEX_MINUS_BINARY, LEX_KEY_OR] do
  begin
    result.addToken(LT(1));
    Consume;
    result.addChild(syn_term);
  end;
end;

function TSpokolParser.syn_sign: TTree;
begin
  result := TTree.Create('sign');
  Consume;
end;

function TSpokolParser.syn_term: TTree;
begin
  result := TTree.Create('term');
  result.addChild(syn_factor);
  while LA(1) in [LEX_MULTIPLY, LEX_DIVIDE, LEX_KEY_AND] do
  begin
    result.addToken(LT(1));
    Consume;
    result.addChild(syn_factor);
  end;
end;

function TSpokolParser.syn_factor: TTree;
begin
  result := TTree.Create('factor');
  case LA(1) of
    LEX_PARENTHESIS_OPEN:
    begin
      Match(LEX_PARENTHESIS_OPEN);
      result.addChild(syn_expr);
      Match(LEX_PARENTHESIS_CLOSE);
    end;
    LEX_KEY_NOT:
    begin
      Consume;
      result.addChild(syn_factor);
    end;
    LEX_MINUS_UNARY:
    begin
      Consume;
      result.addChild(syn_factor);
    end;
    LEX_IDENT,
      LEX_CONST_INT, LEX_CONST_REAL, LEX_CONST_STRING:
      begin
        result.setMainToken(LT(1));
        Consume;
      end;
  end;
end;

end.
