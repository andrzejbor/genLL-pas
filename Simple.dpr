// see ISC_license.txt
{$I genLL.inc}
program Simple;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  Classes,
  SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  SimpleLexer in 'SimpleLexer.pas',
  SimpleParser in 'SimpleParser.pas';

var
  Stream: TFileStream;
  Lex: TSimpleLexer;
  Parser: TSimpleParser;
begin
  Stream := TFileStream.Create('island.in', fmOpenRead or fmShareDenyNone);
  Lex := TSimpleLexer.Create(Stream);
  Parser := TSimpleParser.Create(Lex);
  Parser.pars_program;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
