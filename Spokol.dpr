// see ISC_license.txt
{$I genLL.inc}
program Spokol;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  Classes,
  SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  Trees in 'Trees.pas',
  Tokens in 'Tokens.pas',
  SpokolLexer in 'SpokolLexer.pas',
  SpokolParser in 'SpokolParser.pas';

var
  Stream: TFileStream;
  Lex: TSpokolLexer;
  Parser: TSpokolParser;
  Tree: TTree;
begin
  Stream := TFileStream.Create('test.s', fmOpenRead or fmShareDenyNone);
  Lex := TSpokolLexer.Create(Stream);
  Parser := TSpokolParser.Create(Lex);
  Tree:=Parser.syn_program();
  Tree.print(0);
  Tree.Free;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
