program MakeBigSample;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  SysUtils,
  CMinusMaker in 'CMinusMaker.pas';

var
   maker: TCMinusMaker;
begin
  maker:=TCMinusMaker.Create;
  maker.make_program;
  maker.Free;
end.
