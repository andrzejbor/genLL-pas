// see ISC_license.txt
{$I genLL.inc}
unit XMLLexer;

interface

uses
  Sysutils, Runtime;

const
  LEX_TAG_START_OPEN = 1;
  LEX_TAG_CLOSE = 2;
  LEX_TAG_EMPTY_CLOSE = 3;
  LEX_TAG_END_OPEN = 4;
  LEX_PCDATA = 5; // zrobic
  LEX_GENERIC_ID = 6;
  LEX_WS = 7;
  LEX_ATTR_EQ = 8;
  LEX_ATTR_VALUE = 9; // zrobic

type
  TXMLLexer = class(TLexer)
  protected
    procedure Tokens(); override;
    function GetTokenName(tokenType: integer): string; override;
    function fNAMECHAR(): boolean;
    function fDIGIT(): boolean;
    function fLETTER(): boolean;
    procedure mPCDATA();
    procedure mGENERIC_ID();
    procedure mWS();
    procedure miATTR_VALUE();
  public
    tagMode: boolean;
    elementMode: boolean;
  end;

implementation
uses
  Tokens;

procedure TXMLLexer.Tokens;
var
  LA1: integer;
  lextype: integer;
begin
  LA1 := LA(1);
  if LA1 = CHAR_EOF then
  begin
    ApproveTokenBuf(LEX_EOF, ceDefault);
    exit;
  end;
  if LA(1) = ord('<') then
  begin
    if LA(2) = ord('/') then
    begin
      lextype := LEX_TAG_END_OPEN;
      Consume;
    end else
        lextype := LEX_TAG_START_OPEN;
    tagMode := true;
    Consume;
    ApproveTokenBuf(lextype, ceDefault);
  end
  else if tagMode or not elementMode then
    case LA1 of
      9, 10, 12, 13, ord(' '): mWS();
      ord('A') .. ord('Z'), ord('a') .. ord('z'),
        ord('0') .. ord('9'), ord('_'), ord(':'): mGENERIC_ID();
      ord(''''), ord('"'): miATTR_VALUE();
      ord('>'), ord('/'), ord('='):
        begin
          case LA1 of
            ord('>'): begin lextype := LEX_TAG_CLOSE; tagMode:=false;end;
            ord('/'): if LA(2) = ord('>') then
            begin
               lextype := LEX_TAG_EMPTY_CLOSE;
               tagMode:=false;
               Consume;
            end
            else
                  raise ENoViableAltException.Create('');
            ord('='): lextype := LEX_ATTR_EQ;
          end;
          Consume;
          ApproveTokenBuf(lextype, ceDefault);
        end;
    end
   else mPCDATA();
end;

function TXMLLexer.GetTokenName(tokenType: integer): string;
begin
  case tokenType of
    LEX_TAG_START_OPEN: result := '<';
    LEX_TAG_END_OPEN: result := '</';
    LEX_TAG_CLOSE: result := '>';
    LEX_TAG_EMPTY_CLOSE: result := '/>';
    LEX_PCDATA: result := 'PCData';
    LEX_GENERIC_ID: result := 'genericID';
    LEX_WS: result := 'whiteSpace';
  end;
end;

function TXMLLexer.fLETTER(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('A')) and (ch <= ord('Z'))
    or (ch >= ord('a')) and (ch <= ord('z'))
end;

function TXMLLexer.fNAMECHAR: boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := fLETTER() or fDIGIT() or (ch = ord('.')) or (ch = ord('-'))
    or (ch = ord('_')) or (ch = ord(':'));
end;

function TXMLLexer.fDIGIT(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('0')) and (ch <= ord('9'));
end;

procedure TXMLLexer.mGENERIC_ID;
var
  LA1: integer;
begin
  if fLETTER() or (LA(1) = ord('_')) or (LA(1) = ord(':')) then
      Consume;
  repeat
    LA1 := LA(1);
    if fNAMECHAR() then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_GENERIC_ID, ceDefault);
end;

procedure TXMLLexer.miATTR_VALUE;
var
  LA1: integer;
begin
  LA1 := LA(1);
  if LA1 = ord('"') then
  begin
    MatchOne(ord('"'));
    repeat
      LA1 := LA(1);
      if (LA1 <> CHAR_EOF) and (LA1 <> ord('"')) then
          Consume()
      else
          break;
    until false;
    MatchOne(ord('"'));
  end
  else
  begin
    MatchOne(ord(''''));
    repeat
      LA1 := LA(1);
      if (LA1 <> CHAR_EOF) and (LA1 <> ord('''')) then
          Consume()
      else
          break;
    until false;
    MatchOne(ord(''''));
  end;
  ApproveTokenBuf(LEX_ATTR_VALUE, ceDefault);
end;

procedure TXMLLexer.mPCDATA;
begin
  while (LA(1)<>ord('<')) and (LA(1)<>CHAR_EOF) do
     Consume();
  ApproveTokenBuf(LEX_PCDATA, ceDefault);
end;

procedure TXMLLexer.mWS();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 = 9) or (LA1 = 10) or (LA1 = 12) or (LA1 = 13) or (LA1 = ord(' '))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_WS, ceHidden);
end;

end.
