parser grammar SpokolParser;
options {
    tokenVocab=SpokolLexer;
}


program : PROGRAM IDENT var_seq? proc_seq? instr_block ;
var_seq: var_decl+ ;
var_decl: VAR ident_list COLON ( INT | REAL | BOOL | STRING ) ;
ident_list: IDENT ( COMMA IDENT )*;
proc_seq: proc_decl+ ;
proc_decl: proc_head block ;
proc_head: PROCEDURE IDENT ;
block: var_seq? instr_block ;
instr_block: BEGIN instr_seq? END ;
instr_seq: instr+ ;
instr: assign | print | read | cond | loop | BREAK | proc_call ;
assign: IDENT ASSIGN expr ;
print: PRINT PARENTHESIS_OPEN (expr|STRING_LITERAL) PARENTHESIS_CLOSE ;
read: READ PARENTHESIS_OPEN IDENT PARENTHESIS_CLOSE ;
cond: IF expr THEN instr_seq ( ELSE instr_seq )? ENDIF ;
loop: WHILE expr DO instr_seq ENDWHILE ;
proc_call: IDENT ;
expr: simp_expr ( relop simp_expr )? ;
simp_expr: sign? term ( addop term )* ;
term: factor ( multop factor )* ;
factor: IDENT | STRING_LITERAL | CONST_INT | FLOATING_POINT_LITERAL | NOT factor ;
sign: PLUS | MINUS ;
relop: EQUAL | LESS | GREATER | NOT_EQUAL | LESS_EQUAL | GREATER_EQUAL ;
addop: PLUS | MINUS | OR ;
multop: MULTIPLY | DIVIDE | AND ;

