// see ISC_license.txt
{$I genLL.inc}
program Schrodinger;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  Classes,
  SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  SchrodingerLexer in 'SchrodingerLexer.pas',
  SchrodingerParser in 'SchrodingerParser.pas';

var
  Stream: TFileStream;
  Lex: TSchrodingerLexer;
  Parser: TSchrodingerParser;
begin
  Stream := TFileStream.Create('Schrodinger.in', fmOpenRead or fmShareDenyNone);
  Lex := TSchrodingerLexer.Create(Stream);
  Parser := TSchrodingerParser.Create(Lex);
  Parser.pars_program;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
