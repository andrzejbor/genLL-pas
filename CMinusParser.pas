// see ISC_license.txt
{$I genLL.inc}
unit CMinusParser;

interface

uses
  Runtime, Trees, CMinusLexer;

type
  TCMinusParser = class(TParser)
  public
    function pars_program(): TTree;
    function pars_declaration(): TTree;
    function pars_function(): TTree;
    function pars_stmt(): TTree;
    function pars_assignmentStmt(): TTree;
    function pars_returnStmt(): TTree;
    function pars_expression(): TTree;
    function pars_functionCall(): TTree;
    function pars_term(): TTree;
    function pars_negationExpr(): TTree;
    function pars_multDivExpr(): TTree;
    function pars_addSubExpr(): TTree;
    function pars_paramList(): TTree;
    function pars_callList(): TTree;
  end;

implementation
uses
  Tokens;

function TCMinusParser.pars_program(): TTree;
var
  LA1: integer;
begin
  result := TTree.Create('program');
  repeat
    if (LA(1) = LEX_TYPE) and (LA(2) = LEX_ID) then
    begin
      if LA(3) = LEX_SEMI then
          result.addChild(pars_declaration())
      else if LA(3) = LEX_LPAREN then
          break
      else
      begin
        ErrorP('error in program');
        ConsumeToType(LEX_SEMI);
        Consume(); // SEMI or EOF
      end;
    end
    else if LA(1) = LEX_EOF then
        exit
    else
    begin
      ErrorP('error in program');
      Consume();
    end;
  until false;
  while LA(1) <> LEX_EOF do
      result.addChild(pars_function());
end;

function TCMinusParser.pars_declaration(): TTree;
var
  t0,t1: IToken;
begin
  result := TTree.Create('declaration');
  t0:=Match(LEX_TYPE);
  t1:=Match(LEX_ID);
  Match(LEX_SEMI);
  result.addToken(t0);
  result.addToken(t1);
end;

function TCMinusParser.pars_function(): TTree;
var
  t0,t1: IToken;
begin
  result := TTree.Create('function');
  t0:=Match(LEX_TYPE);
  t1:=Match(LEX_ID);
  Match(LEX_LPAREN);
  result.addToken(t0);
  result.addToken(t1);
  if LA(1) = LEX_TYPE then
    result.addChild(pars_paramList());
  Match(LEX_RPAREN);
  Match(LEX_OPEN_BRACE);
  while LA(1) = LEX_TYPE do
      result.addChild(pars_declaration());
  while LA(1) <> LEX_CLOSE_BRACE do
    result.addChild(pars_stmt());
  Match(LEX_CLOSE_BRACE);
end;

// TYPE ID (COMMA TYPE ID)*
function TCMinusParser.pars_paramList(): TTree;
var
  LA1: integer;
  t0,t1: IToken;
begin
  result := TTree.Create('paramList');
  repeat
    t0:=Match(LEX_TYPE);
    t1:=Match(LEX_ID);
    result.addToken(t0);
    result.addToken(t1);
    if LA(1) <> LEX_COMMA then
        break;
    Match(LEX_COMMA);
  until false;
end;

function TCMinusParser.pars_returnStmt(): TTree;
var
  t0: IToken;
begin
  result := TTree.Create('returnStmt');
  t0:=Match(LEX_RETURN);
  result.addToken(t0);
  result.addChild(pars_expression());
  Match(LEX_SEMI);
end;

function TCMinusParser.pars_stmt(): TTree;
begin
  result:=TTree.Create('pars_stmt');
  case LA(1) of
    LEX_RETURN: result.addChild(pars_returnStmt());
    LEX_ID, LEX_SUB, LEX_INT, LEX_STRING_LITERAL, LEX_LPAREN:
      if LA(2) = LEX_ASSIGN then
          result.addChild(pars_assignmentStmt())
      else
      begin
        result.addChild(pars_expression());
        Match(LEX_SEMI);
      end
  else ErrorP('');
  end;
end;

function TCMinusParser.pars_term(): TTree;
var
  t0: IToken;
begin
  result:=TTree.Create('term');
  t0:=LT(1);
  case t0.getType of
    LEX_INT:
    begin
      Consume;
      result.addToken(t0);
    end;
    LEX_STRING_LITERAL:
    begin
      Consume;
      result.addToken(t0);
    end;
    LEX_ID:
      begin
        result.addToken(t0);
        if LA(2) = LEX_LPAREN then
            result.addChild(pars_functionCall())
        else
            Consume;
      end;
    LEX_LPAREN:
      begin
        Match(LEX_LPAREN);
        result.addChild(pars_expression());
        Match(LEX_RPAREN);
      end;
  end;
end;

function TCMinusParser.pars_expression(): TTree;
begin
  result:=TTree.Create('expression');
  result.addChild(pars_addSubExpr());
end;

//ID LPAREN callList? RPAREN
function TCMinusParser.pars_functionCall(): TTree;
var
  t0: IToken;
begin
  result:=TTree.Create('functionCall');
  t0:=Match(LEX_ID);
  result.addToken(t0);
  Match(LEX_LPAREN);
  if LA(1)<>LEX_RPAREN then
    result.addChild(pars_callList());
  Match(LEX_RPAREN);
end;

function TCMinusParser.pars_multDivExpr(): TTree;
var
  t0: IToken;
begin
  result:=TTree.Create('multDivExpr');
  result.addChild(pars_negationExpr());
  if LA(1) = LEX_MUL then
  begin
      t0:=Consume;
      result.addToken(t0);
  end else if LA(1) = LEX_DIV then
  begin
      t0:=Consume;
      result.addToken(t0);
  end else
      exit;
  result.addChild(pars_negationExpr());
end;

function TCMinusParser.pars_negationExpr(): TTree;
var
  t0: IToken;
begin
  result:=TTree.Create('negationExpr');
  if LA(1) = LEX_SUB then
  begin
    t0:=Consume;
    result.addToken(t0);
  end;
  result.addChild(pars_term());
end;

function TCMinusParser.pars_addSubExpr(): TTree;
var
  t0: IToken;
begin
  result:=TTree.Create('addSubExpr');
  result.addChild(pars_multDivExpr());
  if LA(1) = LEX_ADD then
  begin
    t0:=Consume;
    result.addToken(t0);
  end
  else if LA(1) = LEX_SUB then
  begin
    t0:=Consume;
    result.addToken(t0);
  end
  else
      exit;
  result.addChild(pars_multDivExpr());
end;

function TCMinusParser.pars_assignmentStmt(): TTree;
var
  t0: IToken;
begin
  result:=TTree.Create('assignmentStmt');
  t0:=Match(LEX_ID);
  result.addToken(t0);
  t0:=Match(LEX_ASSIGN);
  result.addToken(t0);
  result.addChild(pars_expression());
  Match(LEX_SEMI);
end;

function TCMinusParser.pars_callList(): TTree;
begin
  result:=TTree.Create('callList');
  repeat
    result.addChild(pars_expression());
    if LA(1) <> LEX_COMMA then
        break;
    Match(LEX_COMMA);
  until false;
end;

end.
