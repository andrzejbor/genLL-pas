// see ISC_license.txt
{$I genLL.inc}
unit CMinusLexer;

interface

uses
  Sysutils, Runtime;

const
  // LEX_CALL = 4; // 'call'
  LEX_CLOSE_BRACE = 5;
  LEX_COMMA = 6;
  LEX_COMMENT = 7;
  LEX_DECLS = 8;
  LEX_DEF = 9;
  LEX_DEFUN = 10;
  LEX_DIGIT = 11;
  LEX_ENDL = 12;
  LEX_FUNCS = 13;
  LEX_ID = 14;
  LEX_INT = 15;
  LEX_LETTER = 16;
  LEX_LPAREN = 17;
//  LEX_NEGATE = 18;
  LEX_OPEN_BRACE = 19;
  LEX_PARAMS = 20;
  // LEX_PROGRAM = 21; // 'program'
  LEX_RPAREN = 22;
  LEX_SEMI = 23; // ';'
  LEX_STRING_LITERAL = 24;
  LEX_TYPE = 25;
  LEX_WS = 26;
  LEX_MUL = 27; // '*'
  LEX_ADD = 28; // '+'
  LEX_SUB = 29; // '-'
  LEX_DIV = 30; // '/'
  LEX_ASSIGN = 31; // '='
  LEX_RETURN = 32; // 'return'

type
  TCMinusLexer = class(TLexer)
  protected
    procedure Tokens(); override;
    function GetTokenName(tokenType: integer): string; override;
    function fENDL(): boolean;
    function fLETTER(): boolean;
    function fDIGIT(): boolean;
    procedure mTYPE();
    procedure mWS();
    procedure mCOMMENT();
    procedure mID();
    procedure mSTRING_LITERAL();
    procedure mINT();
    procedure mRETURN();
  end;

implementation
uses
  Tokens;

procedure TCMinusLexer.Tokens;
var
  LA1: integer;
  lextype: integer;
begin
  LA1 := LA(1);
  if LA1 = LEX_EOF then
  begin
    ApproveTokenBuf(LEX_EOF, ceDefault);
    exit;
  end;
try
  case LA1 of
    9 .. 10, 13, ord(' '): mWS();
    ord('A') .. ord('Z'), ord('a') .. ord('z'), ord('_'):
      begin
        if LA1 = ord('i') then
        begin
          if CompareLatin('int') then
              mTYPE()
          else
              mID();
        end
        else
          if LA1 = ord('s') then
        begin
          if CompareLatin('string') then
              mTYPE()
          else
              mID();
        end
        else
          if LA1 = ord('r') then
        begin
          if CompareLatin('return') then
              mRETURN()
          else
              mID();
        end
        else
            mID();
      end;
    ord('0') .. ord('9'): mINT();
    ord('/'): if LA(2) = ord('/') then
          mCOMMENT()
      else
      begin
        Consume;
        ApproveTokenBuf(LEX_DIV, ceDefault);
      end;

    ord('"'): mSTRING_LITERAL();

    ord(';'), ord('*'), ord('+'), ord('-'),
      ord('='),ord('('),ord(')'),
      ord('{'),ord('}'), ord(','):
      begin
        case LA1 of
          ord(';'): lextype := LEX_SEMI;
          ord('*'): lextype := LEX_MUL;
          ord('+'): lextype := LEX_ADD;
          ord('-'): lextype := LEX_SUB;
          ord('/'): lextype := LEX_DIV;
          ord('='): lextype := LEX_ASSIGN;
          ord('{'): lextype := LEX_OPEN_BRACE;
          ord('}'): lextype := LEX_CLOSE_BRACE;
          ord('('): lextype := LEX_LPAREN;
          ord(')'): lextype := LEX_RPAREN;
          ord(','): lextype := LEX_COMMA;
        end;
        Consume;
        ApproveTokenBuf(lextype, ceDefault);
      end;
  end;
except
  on E: EMismatchedCharException do ErrorL(E.Message);
end;
end;

function TCMinusLexer.GetTokenName(tokenType: integer): string;
begin
  { case tokenType of
    LEX_ID: result := 'Ident';
    LEX_INT: result := 'INT';
    LEX_WS: result := 'white space';
    LEX_4: result := '(';
    LEX_5: result := ')';
    LEX_6: result := '*';
    LEX_7: result := '+';
    LEX_8: result := '-';
    LEX_9: result := ';';
    LEX_10: result := '=';
    end; }
end;

function TCMinusLexer.fENDL(): boolean;
begin
  result := (LA(1) = 13) and (LA(2) = 10) or (LA(1) = 10);
end;

function TCMinusLexer.fLETTER(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('A')) and (ch <= ord('Z'))
    or (ch >= ord('a')) and (ch <= ord('z'))
end;

function TCMinusLexer.fDIGIT(): boolean;
var
  ch: integer;
begin
  ch := LA(1);
  result := (ch >= ord('0')) and (ch <= ord('9'));
end;

procedure TCMinusLexer.mWS();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if LA1 = -1 then exit;
    if (LA1 = 9) or fENDL() or (LA1 = ord(' '))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_WS, ceHidden);
end;

procedure TCMinusLexer.mCOMMENT;
var
  LA1: integer;
begin
  MatchLatin('//');
  repeat
    LA1 := LA(1);
    if (LA1 <> -1) and not fENDL() then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_COMMENT, ceHidden);
end;

procedure TCMinusLexer.mID();
var
  LA1: integer;
begin
  if fLETTER() or (LA(1)=ord('_')) then
    Consume;
  repeat
    LA1 := LA(1);
    if fLETTER() or (LA(1)=ord('_')) or fDigit() then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_ID, ceDefault);
end;

procedure TCMinusLexer.mINT();
var
  LA1: integer;
begin
  if LA(1) = ord('0') then
      Consume()
  else
  begin
    Consume(); // 1..9
    while fDIGIT() do
        Consume();
  end;
  ApproveTokenBuf(LEX_INT, ceDefault);
end;

procedure TCMinusLexer.mRETURN;
begin
  MatchLatin('return');
  ApproveTokenBuf(LEX_RETURN, ceDefault);
end;

procedure TCMinusLexer.mSTRING_LITERAL;
var
  LA1: integer;
begin
  MatchOne(ord('"'));
  repeat
    LA1 := LA(1);
    if (LA1 > 0) and (LA1 <> ord('"')) and (LA1 <> 10) and
      (LA1 <> 13) then
        Consume()
    else if (LA1 = ord('"')) and (LA(2) = ord('"')) then
        Consume(2) // two "
    else
        break;
  until false;
  MatchOne(ord('"'));
  ApproveTokenBuf(LEX_STRING_LITERAL, ceDefault);
end;

procedure TCMinusLexer.mTYPE;
begin
  if LA(1) = ord('i') then
      MatchLatin('int')
  else
      MatchLatin('string');
  ApproveTokenBuf(LEX_TYPE, ceDefault);
end;

end.
