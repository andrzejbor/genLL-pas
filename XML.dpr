// see ISC_license.txt
{$I genLL.inc}
program XML;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  Classes,
  SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  XMLLexer in 'XMLLexer.pas',
  XMLParser in 'XMLParser.pas',
  Trees in 'Trees.pas',
  Tokens in 'Tokens.pas';

var
  Stream: TFileStream;
  Lex: TXMLLexer;
  Parser: TXMLParser;
begin
  Stream := TFileStream.Create('sample.xml', fmOpenRead or fmShareDenyNone);
  Lex := TXMLLexer.Create(Stream);
  Parser := TXMLParser.Create(Lex);
  Parser.pars_document;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
