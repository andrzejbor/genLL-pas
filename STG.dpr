// see ISC_license.txt
{$I genLL.inc}
program STG;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  Classes,
  SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  Trees in 'Trees.pas',
  Tokens in 'Tokens.pas',
  STGLexer in 'STGLexer.pas',
  STGParser in 'STGParser.pas';

var
  Stream: TFileStream;
  Lex: TSTGLexer;
  Parser: TSTGParser;
  Tree: TTree;
begin
  Stream := TFileStream.Create('test.stg', fmOpenRead or fmShareDenyNone);
  Lex := TSTGLexer.Create(Stream);
  Parser := TSTGParser.Create(Lex);
  Tree:=Parser.syn_program();
  Tree.print(0);
  Tree.Free;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
