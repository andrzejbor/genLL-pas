// see ISC_license.txt
{$I genLL.inc}
program genLL;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Classes, SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  GramLexer in 'GramLexer.pas',
  GramParser in 'GramParser.pas';


var
  Stream: TFileStream;
  Lex: TGramLexer;
  Parser: TGramParser;
begin
  Stream := TFileStream.Create('badEduExamples.in', fmOpenRead or fmShareDenyNone);
  Lex := TGramLexer.Create(Stream);
  Parser := TGramParser.Create(Lex);
  Parser.document;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
