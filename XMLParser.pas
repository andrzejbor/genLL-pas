// see ISC_license.txt
{$I genLL.inc}
unit XMLParser;

interface

uses
  Runtime, XMLLexer;

const
  PARS_STARTTAG = 1;
  PARS_EMPTYELEMENT = 2;
type
  TXMLParser = class(TParser)
  public
    function twoWay(): integer;
    procedure pars_document;
    procedure pars_element;
    //procedure pars_startTag;
    procedure pars_attribute;
    procedure pars_endTag;
    //procedure pars_emptyElement;
  end;

implementation

{ TXMLParser }
//startTag  : TAG_START_OPEN GENERIC_ID (attribute)* TAG_CLOSE ;
//emptyElement : TAG_START_OPEN GENERIC_ID  (attribute)* TAG_EMPTY_CLOSE ;
function TXMLParser.twoWay: integer;
var
  LA1: integer;
begin
  Match(LEX_TAG_START_OPEN);
  Match(LEX_GENERIC_ID);
  while LA(1)=LEX_GENERIC_ID do
    pars_attribute();
  LA1 := LA(1);
  if LA1 = LEX_TAG_CLOSE then
  begin
    Match(LEX_TAG_CLOSE);
    result := 1;
  end else
   if LA1 = LEX_TAG_EMPTY_CLOSE then
   begin
     Match(LEX_TAG_EMPTY_CLOSE);
     result := 2;
   end else ErrorP('');
end;

procedure TXMLParser.pars_document;
begin
try
  repeat
    (FLexer as TXMLLexer).elementMode:=true;
    pars_element();
    (FLexer as TXMLLexer).elementMode:=false;
  until LA(1)=LEX_EOF;
 except
   on E: ETokenEOFException do writeln('Unexpected EOF');
 end;
end;

procedure TXMLParser.pars_element;
begin
  case twoWay() of
    1:
    begin
      while LA(1) in [LEX_TAG_START_OPEN, LEX_PCDATA] do
       if LA(1)=LEX_TAG_START_OPEN then
         pars_element()
       else
         Match(LEX_PCDATA);
      pars_endTag();
    end;
    2: ;
  end;
end;


procedure TXMLParser.pars_attribute;
begin
  Match(LEX_GENERIC_ID);
  Match(LEX_ATTR_EQ);
  Match(LEX_ATTR_VALUE);
end;


procedure TXMLParser.pars_endTag;
begin
  Match(LEX_TAG_END_OPEN);
  Match(LEX_GENERIC_ID);
  Match(LEX_TAG_CLOSE);
end;

end.
