// see ISC_license.txt
{$I genLL.inc}
program CMinus;

{$APPTYPE CONSOLE}

{$R *.res}


uses
  Classes, SysUtils,
  nQueues in 'nQueues.pas',
  Runtime in 'Runtime.pas',
  Trees in 'Trees.pas',
  Tokens in 'Tokens.pas',
  CMinusLexer in 'CMinusLexer.pas',
  CMinusParser in 'CMinusParser.pas';

var
  Stream: TFileStream;
  Lex: TCMinusLexer;
  Parser: TCMinusParser;
  Tree: TTree;
begin
  Stream := TFileStream.Create('test.cm', fmOpenRead or fmShareDenyNone);
  Lex := TCMinusLexer.Create(Stream);
  Parser := TCMinusParser.Create(Lex);
  Tree:=Parser.pars_program();
  Tree.print(0);
  Tree.Free;
  Parser.Free;
  Lex.Free;
  Stream.Free;
end.
