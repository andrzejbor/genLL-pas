// see ISC_license.txt
{$I genLL.inc}
unit STLexer;

interface

uses
  SysUtils, Runtime;

const
  LEX_WS             =  1;
  LEX_COMMENT        =  2;
  LEX_DELIMITER      =  3;
  LEX_IDENT          =  4;
  LEX_KEY_IF         =  5;
  LEX_KEY_ELSE       =  6;
  LEX_KEY_ENDIF      =  7;



(*  LEX_IDENT         =  3;

  LEX_CONST_INT     =  5;
  LEX_CONST_REAL    =  6;
  LEX_CONST_STRING  =  7;

  LEX_ASSIGN        =  8; // :=
  LEX_EQUAL         =  9; // =
  LEX_NOT_EQUAL     = 10; // !=
  LEX_LESS          = 11; // <
  LEX_GREATER       = 12; // >
  LEX_LESS_EQUAL    = 13; // <=
  LEX_GREATER_EQUAL = 14; // >=
  LEX_PLUS          = 15; // +
	LEX_MINUS_BINARY  = 16;		// -
	LEX_MINUS_UNARY   = 17;		// -
  LEX_MULTIPLY      = 18; // *
  LEX_DIVIDE        = 19; // /

  //LEX_BRACE_OPEN    = 20; // {
  //LEX_BRACE_CLOSE   = 21; // }
  LEX_PARENTHESIS_OPEN  = 22; // (
  LEX_PARENTHESIS_CLOSE = 23; // )
  LEX_COMMA         = 24; // ,
  LEX_DOT           = 25; // .
  LEX_SEMICOLON     = 26; // ;
  LEX_COLON         = 27; // :

  LEX_KEY_AND       = 28;
  LEX_KEY_BEGIN     = 29;
  LEX_KEY_BOOL      = 30;
  LEX_KEY_BREAK     = 31;
  LEX_KEY_DO        = 32;
  LEX_KEY_ELSE      = 33;
  LEX_KEY_END       = 34;
  LEX_KEY_ENDIF     = 35;
  LEX_KEY_ENDWHILE  = 36;
  LEX_KEY_FALSE     = 37;
  LEX_KEY_IF        = 38;
  LEX_KEY_INT       = 39;
  LEX_KEY_NOT       = 40;
  LEX_KEY_OR        = 41;
  LEX_KEY_PRINT     = 42;
  LEX_KEY_PROCEDURE = 43;
  LEX_KEY_PROGRAM   = 44;
  LEX_KEY_READ      = 45;
  LEX_KEY_REAL      = 46;
  LEX_KEY_STRING    = 47;
  LEX_KEY_THEN      = 48;
  LEX_KEY_TRUE      = 49;
  LEX_KEY_VAR       = 50;
  LEX_KEY_WHILE     = 51;*)

type
  TSTLexer = class(TLexer)
  protected
    function fEndL(LA1: integer): boolean; {$IFDEF USEINLINE}inline; {$ENDIF}
    function fLetter(LA1: integer): boolean; {$IFDEF USEINLINE}inline; {$ENDIF}
    function fDigit(LA1: integer): boolean; {$IFDEF USEINLINE}inline; {$ENDIF}
    procedure mWS();
    procedure mCOMMENT();
    function Keywords(LA1: integer): Boolean;
    procedure mIDENT();
    procedure Tokens(); override;
    (*procedure OnCreate(); override;


    function GetTokenName(tokenType: integer): string; override;

    function fEscapeSequence(LA1: integer; out altlen: integer): boolean;
    procedure twoWay();                     *)

//
    {}
  public
  end;

implementation

uses
  Tokens;

function TSTLexer.fEndL(LA1: integer): boolean;
begin
  result := LA1 in [10,13];
end;

function TSTLexer.fLetter(LA1: integer): boolean;
begin
  result := LA1 in [ord('A')..ord('Z'),ord('a')..ord('z')];
end;

function TSTLexer.fDigit(LA1: integer): boolean;
begin
  result := LA1 in [ord('0')..ord('9')];
end;

procedure TSTLexer.mWS();
var
  LA1: integer;
begin
  repeat
    LA1 := LA(1);
    if (LA1 = 9) or fENDL(LA1) or (LA1 = ord(' '))
    then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_WS, ceHidden);
end;

procedure TSTLexer.mCOMMENT;
var
  LA1: integer;
begin
  MatchLatin('<!');
  repeat
    LA1 := LA(1);
    if LA1 = CHAR_EOF then
        raise ETokenEOFException.Create('');
    if LA1 <> ord('!') then
        Consume()
    else
    begin
      if LA(2) = ord('>') then
          break
      else
          Consume(2);
    end;
  until false;
  ApproveTokenBuf(LEX_COMMENT, ceHidden);
end;

function TSTLexer.Keywords(LA1: integer): Boolean;
var
  lextype: integer;
begin
  result := false;
  case LA1 of
    ord('e'):
      if CompareConsumeLatin('else') then
          lextype := LEX_KEY_ELSE
      else
        if CompareConsumeLatin('endif') then
          lextype := LEX_KEY_ENDIF
      else
          exit;
    ord('i'):
      if CompareConsumeLatin('if') then
          lextype := LEX_KEY_IF
      else
          exit;
  else exit;
  end;
  result := true;
  ApproveTokenBuf(lextype, ceDefault);
end;

procedure TSTLexer.mIDENT();
var
  LA1: integer;
begin
  if fLETTER(LA1) or (LA(1) = ord('_')) then
      Consume;
  repeat
    LA1 := LA(1);
    if fLETTER(LA1) or (LA(1) = ord('_'))
      or (LA(1) = ord('-')) or fDIGIT(LA1) then
        Consume()
    else
        break;
  until false;
  ApproveTokenBuf(LEX_IDENT, ceDefault);
end;


                   (*


function TSTLexer.GetTokenName(tokenType: integer): string;
begin
  case tokenType of
    LEX_WHITESPACE: result:='whitespace';
    LEX_COMMENT: result:='comment';
    LEX_IDENT: result:='ident';

    LEX_CONST_INT: result:='const int';
    LEX_CONST_REAL: result:='const real';
    LEX_CONST_STRING: result:='const string';

    LEX_ASSIGN: result:=':=';
    LEX_EQUAL: result:='=';
    LEX_NOT_EQUAL: result:='!=';
    LEX_LESS: result:='<';
    LEX_GREATER: result:='>';
    LEX_LESS_EQUAL: result:='<=';
    LEX_GREATER_EQUAL: result:='>=';
    LEX_PLUS: result:='+';
    LEX_MINUS_BINARY: result:='minus binary';
    LEX_MINUS_UNARY: result:='minus unary';
    LEX_MULTIPLY: result:='*';
    LEX_DIVIDE: result:='/';

    //LEX_BRACE_OPEN: result:='{';
    //LEX_BRACE_CLOSE: result:='}';
    LEX_PARENTHESIS_OPEN: result:='(';
    LEX_PARENTHESIS_CLOSE: result:=')';
    LEX_COMMA: result:=',';
    LEX_DOT: result:='.';
    LEX_SEMICOLON: result:=';';
    LEX_COLON: result:=':';

    LEX_KEY_AND: result:='and';
    LEX_KEY_BEGIN: result:='begin';
    LEX_KEY_BOOL: result:='bool';
    LEX_KEY_BREAK: result:='break';
    LEX_KEY_DO: result:='do';
    LEX_KEY_ELSE: result:='else';
    LEX_KEY_END: result:='end';
    LEX_KEY_ENDIF: result:='endif';
    LEX_KEY_ENDWHILE: result:='endwhile';
    LEX_KEY_FALSE: result:='false';
    LEX_KEY_IF: result:='if';
    LEX_KEY_INT: result:='int';
    LEX_KEY_NOT: result:='not';
    LEX_KEY_OR: result:='or';
    LEX_KEY_PRINT: result:='print';
    LEX_KEY_PROCEDURE: result:='procedure';
    LEX_KEY_PROGRAM: result:='program';
    LEX_KEY_READ: result:='read';
    LEX_KEY_REAL: result:='real';
    LEX_KEY_STRING: result:='string';
    LEX_KEY_THEN: result:='then';
    LEX_KEY_TRUE: result:='true';
    LEX_KEY_VAR: result:='var';
    LEX_KEY_WHILE: result:='while';
  end;
end;

function TSpokolLexer.fEndL(LA1: integer): boolean;
begin
  result := LA1 in [10,13];
end;

function TSpokolLexer.fLetter(LA1: integer): boolean;
begin
  result := LA1 in [ord('A')..ord('Z'),ord('a')..ord('z')];
end;

function TSpokolLexer.fDigit(LA1: integer): boolean;
begin
  result := LA1 in [ord('0')..ord('9')];
end;

function TSTLexer.fEndL(LA1: integer; out altlen: integer): boolean;
begin
  if (LA1 = 13) and (LA(2)=10) then
  begin
    result:=true;
    altlen:=2;
  end else if LA1 = 10 then
    result:=true
  else
    result:=false;
end;

function TSTLexer.fLetter(LA1: integer): boolean;
begin
  result := (LA1 in [ord('A')..ord('Z')]) or (LA1 in [ord('a')..ord('z')])
end;

procedure TSTLexer.OnCreate();
begin
  PrevType := LEX_EOF;
end;

function TSTLexer.fDigit(LA1: integer): boolean;
begin
  result := LA1 in [ord('0')..ord('9')];
end;

function TSTLexer.fEscapeSequence(LA1: integer; out altlen: integer): boolean;
begin
  result := (LA1=ord('\')) and (LA(2) in [ord('b'),ord('t'),ord('n'),ord('f'),ord('r'),
                                       ord('"'),ord(''''),ord('\')]);
  altlen := 2;
end;




procedure TSTLexer.twoWay();
var
  lextype: integer;
  LA1: integer;
begin
  case LA(1) of
    ord('0'):
    begin
      Consume;
      if fDigit(LA(1)) then  ENoViableAltException.Create('');
    end;
    ord('1')..ord('9'):
    begin
      Consume;
      while fDigit(LA(1)) do Consume;
    end;
  end;
  lextype:=LEX_CONST_INT;
  if LA(1)=ord('.') then
  begin
    Consume;
    while fDigit(LA(1)) do Consume;
    lextype:=LEX_CONST_REAL;
  end;
  if LA(1) in [ord('e'),ord('E')] then
  begin
    Consume;
    if LA(1) in [ord('+'),ord('-')] then Consume;
    while fDigit(LA(1)) do Consume;
    lextype:=LEX_CONST_REAL;
  end;
  ApproveTokenBuf(lextype, ceDefault);
end;

    *)
procedure TSTLexer.Tokens;
var
  LA1: integer;
  lextype: integer;
begin
  LA1 := LA(1);
  if LA1 = LEX_EOF then
  begin
    ApproveTokenBuf(LEX_EOF, ceDefault);
    exit;
  end;
  try
    case LA1 of
      9, 10, 13, ord(' '): mWS();

      ord('/'): if LA(2) = ord('/') then
          mCOMMENT()
      else
          raise ENoViableAltException.Create('');

      ord('A') .. ord('Z'), ord('a') .. ord('z'), ord('_'):
        if not Keywords(LA1) then mIDENT();
      else raise ENoViableAltException.Create('');
    end;
  except
    on E: EMismatchedCharException do
      ErrorL(E.Message);
  end;
end;

end.
