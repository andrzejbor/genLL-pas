{$DEFINE EDU}// EDU : many small grammars in one file

{$IFDEF fpc}
  {$DEFINE USEINLINE}
  {$MODE Delphi}
{$ELSE}
  // since Delphi 2007
  {$IF Declared(CompilerVersion) and (CompilerVersion>=18.50)}
    {$DEFINE USEINLINE}
  {$IFEND}
{$ENDIF}
